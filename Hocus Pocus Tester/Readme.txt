How to play the tester:

The board has these tiles:
-Player: Represented as Chip.
-White Tiles: Represents walkable tiles.
-Black Tiles: Represents wall tiles.
-Grey Tiles: Represents wall trap tiles; they turn into walls once walked over.
-Yellow Tiles: Represents the win tile; resets level once walked over.

Controls:
-Keyboard:
 -Arrow keys to move.
 -'Z' for CCW rotation.  'X' for CW rotation.
-Mouse:
 -Click and hold to move.
 -Click Chip to activate rotation mode.  Click the rotation vortex to deactivate it.  In rotation mode click and drag the mouse in the direction you want to rotate the level in.

Notes:
-Four tiles must be present at all times.  No more, no less.
-Roatations only work when all four tiles are squares and are all the same sizes.
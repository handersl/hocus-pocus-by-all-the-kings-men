package hocusPocusTesterPackage;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;

import javax.swing.ImageIcon;

public class Player
{
	//This Rect. is where the player is and the amount of space the character
	//takes up.
	private Rectangle boundingBox;
	//The sprite for the Player.
	private Image playerSprite;
	
	//The dimensions of the Player.
	private static final int PLAYER_WIDTH = 32;
	private static final int PLAYER_HEIGHT = 32;
	//The Player art filename.
    private static final String PLAYER_SPRITE_FILENAME = "Art/Chip.png";
    
	//Create the Player at (0,0) and load the image needed.
	public Player()
	{
		boundingBox = new Rectangle(0, 0, PLAYER_WIDTH, PLAYER_HEIGHT);
		
		playerSprite = new ImageIcon(PLAYER_SPRITE_FILENAME).getImage();
	}
	
	//Move the Player to (newXPosition,newYPosition).
	public void moveTo(int newXPosition, int newYPosition)
	{
		boundingBox = new Rectangle(newXPosition, newYPosition, PLAYER_WIDTH, PLAYER_HEIGHT);
	}
	
	public void update()
	{
	}
	
	//Override the paint method to draw the Player sprite at the right place.
	public void paint(Graphics mainGraphic)
	{
        Graphics2D playerGraphic = (Graphics2D)mainGraphic;
        playerGraphic.drawImage(playerSprite, boundingBox.x, boundingBox.y, null);
    }
	
	public Rectangle getBoundingBox()
	{
		return boundingBox;
	}
}

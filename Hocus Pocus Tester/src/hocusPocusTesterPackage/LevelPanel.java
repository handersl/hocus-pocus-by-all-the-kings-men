package hocusPocusTesterPackage;

import hocusPocusTesterPackage.Tiles.BaseTile;
import hocusPocusTesterPackage.Tiles.TileWalkable;
import hocusPocusTesterPackage.Tiles.TileWall;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;

//The Level Panel class houses the tiles that are local to each other.
//When the level roates these tiles stay at the same distance for
//one another.
public class LevelPanel 
{
	//The origin of a Level Plane is the top left of all the tiles.
	//It is the local origin of the Level Plane.
	private Point origin;
	//The size of the Level Panel.  Units of tiles.
	private int width;
	private int height;

	//The point that this panel wants to move to,
	private Point destination;
	//The speed of the panel.
	private double speed;
	//Is this panel done moving.
	private boolean doneMoving;
	
	//The Rectangle area in pixels.
	private Rectangle boundingBoxArea;
	
	//The list of the actual tiles.  It is a one dimensional array so
	//math will be done to navigate it.
	private ArrayList<BaseTile> listOfTiles;
	
	//How close does a panel have to be to snap to position.
	private static final int THRESHOLD_TO_SNAP = 10;
	
	//Creates a Level Plane with all Tile Walkables.  W is the numer of tiles
	//across and h is the number of tiles down.
	public LevelPanel(int xOrigin, int yOrigin, int levelPanelW, int levelPanelH)
	{
		origin = new Point(xOrigin, yOrigin);
	
		destination = new Point(xOrigin, yOrigin);
		speed = 1;
		
		doneMoving = true;
		
		listOfTiles = new ArrayList<BaseTile>();
		
		width = levelPanelW;
		height = levelPanelH;
		
		boundingBoxArea = new Rectangle((int)origin.getX(), (int)origin.getY(), width * GlobalConstants.getTileSize(), height * GlobalConstants.getTileSize());
		
		int currentlyBeingFilledX = 0;
		int currentlyBeingFilledY = 0;
		for(int currentTileBeingFilled = 0; currentTileBeingFilled < width*height; currentTileBeingFilled++)
		{
			listOfTiles.add(new TileWalkable((int)(origin.getX() + currentlyBeingFilledX*GlobalConstants.getTileSize()), (int)(origin.getY() + currentlyBeingFilledY*GlobalConstants.getTileSize())));
			currentlyBeingFilledX++;
			if(currentlyBeingFilledX >= width)
			{
				currentlyBeingFilledX = 0;
				currentlyBeingFilledY++;
			}
		}
	}
	
	public LevelPanel(LevelPanel oldLevelPanel)
	{
		origin = new Point((int)(oldLevelPanel.getBoundingBoxArea().getX()), (int)(oldLevelPanel.getBoundingBoxArea().getY()));
		
		destination = new Point((int)(oldLevelPanel.getBoundingBoxArea().getX()), (int)(oldLevelPanel.getBoundingBoxArea().getY()));
		speed = 1;
		
		doneMoving = true;
		
		listOfTiles = new ArrayList<BaseTile>();
		
		width = (int)(oldLevelPanel.getBoundingBoxArea().getWidth()/GlobalConstants.getTileSize());
		height = (int)(oldLevelPanel.getBoundingBoxArea().getHeight()/GlobalConstants.getTileSize());
		
		boundingBoxArea = new Rectangle((int)origin.getX(), (int)origin.getY(), width * GlobalConstants.getTileSize(), height * GlobalConstants.getTileSize());
		
		for(int copyTileNumber = 0;copyTileNumber < oldLevelPanel.getListOfTiles().size(); copyTileNumber++)
		{
			BaseTile baseTile = oldLevelPanel.getListOfTiles().get(copyTileNumber);
			listOfTiles.add(baseTile.deepCopy());
		}
	}
	
	//Replace the tile at (xPos, yPos).  The position of the new tile
	//does not matter because it will be set by this method to fit where
	//it should belong.  The location is 1-indexed and is the number of tiles to move
	//from the origin of the Level Panel.
	public void addTile(int xPos, int yPos, BaseTile newTile)
	{
		newTile.moveTo((int)(origin.getX() + xPos * GlobalConstants.getTileSize()), (int)(origin.getY() + yPos* GlobalConstants.getTileSize()));
		listOfTiles.set(xPos + yPos*width, newTile);
	}
	
	//This fills the board with the wall tile if placed.
	public void fillWall()
	{
		listOfTiles.clear();
		int currentlyBeingFilledX = 0;
		int currentlyBeingFilledY = 0;
		for(int currentTileBeingFilled = 0; currentTileBeingFilled < width*height; currentTileBeingFilled++)
		{
			listOfTiles.add(new TileWall((int)(origin.getX() + currentlyBeingFilledX*GlobalConstants.getTileSize()), (int)(origin.getY() + currentlyBeingFilledY*GlobalConstants.getTileSize())));
			currentlyBeingFilledX++;
			if(currentlyBeingFilledX >= width)
			{
				currentlyBeingFilledX = 0;
				currentlyBeingFilledY++;
			}
		}
	}
	
	//Sets the panel to move towards this point.
	public void moveTo(int newXPosition, int newYPosition)
	{
		destination = new Point(newXPosition, newYPosition);
	}
	
	//This will move all the entire Level Panel to a new origin point.
	public void moveToImm(int newXPosition, int newYPosition)
	{
		int xPositionDifference = (int)(newXPosition - origin.getX());
		int yPositionDifference = (int)(newYPosition - origin.getY());
		
		origin = new Point(newXPosition, newYPosition);
		boundingBoxArea = new Rectangle((int)origin.getX(), (int)origin.getY(), width * GlobalConstants.getTileSize(), height * GlobalConstants.getTileSize());
	
		for(int tileNumber = 0;tileNumber < listOfTiles.size(); tileNumber++)
		{
			BaseTile baseTile = listOfTiles.get(tileNumber);
			baseTile.moveTo((int)(baseTile.getBoundingBox().getX() + xPositionDifference), (int)(baseTile.getBoundingBox().getY() + yPositionDifference));
		}
	}
	
	public void update()
	{
		for(int tileNumber = 0;tileNumber < listOfTiles.size(); tileNumber++)
		{
			BaseTile baseTile = listOfTiles.get(tileNumber);
			baseTile.update();
		}
		
		//Move the level panel by its speed towards its point.
		if(!destination.equals(origin))
		{
			doneMoving = false;
			double xDiff = destination.getX() - origin.getX();
			double yDiff = destination.getY() - origin.getY();

			//Move in the y direction.
			if(Double.compare(xDiff, 0.0) == 0)
			{
				int sign = 1;
				if(yDiff < 0)
				{
					sign = -1;
				}
				origin = new Point((int)(origin.getX()), (int)(origin.getY() + speed*sign));
				boundingBoxArea = new Rectangle((int)origin.getX(), (int)origin.getY(), width * GlobalConstants.getTileSize(), height * GlobalConstants.getTileSize());
			
				for(int tileNumber = 0;tileNumber < listOfTiles.size(); tileNumber++)
				{
					BaseTile baseTile = listOfTiles.get(tileNumber);
					baseTile.moveTo((int)(baseTile.getBoundingBox().getX()), (int)(baseTile.getBoundingBox().getY() + speed*sign));
				}
				if(Math.abs(destination.getY() - origin.getY()) < THRESHOLD_TO_SNAP)
				{
					moveToImm((int)destination.getX(), (int)destination.getY());
				}
			}
			//Move in the x direction.
			else if(Double.compare(yDiff, 0.0) == 0)
			{
				int sign = 1;
				if(xDiff < 0)
				{
					sign = -1;
				}
				origin = new Point((int)(origin.getX() + speed*sign), (int)(origin.getY()));
				boundingBoxArea = new Rectangle((int)origin.getX(), (int)origin.getY(), width * GlobalConstants.getTileSize(), height * GlobalConstants.getTileSize());
			
				for(int tileNumber = 0;tileNumber < listOfTiles.size(); tileNumber++)
				{
					BaseTile baseTile = listOfTiles.get(tileNumber);
					baseTile.moveTo((int)(baseTile.getBoundingBox().getX() + speed*sign), (int)(baseTile.getBoundingBox().getY()));
				}
				
				if(Math.abs(destination.getX() - origin.getX()) < THRESHOLD_TO_SNAP)
				{
					moveToImm((int)destination.getX(), (int)destination.getY());
				}
			}
		}
		else
		{
			doneMoving = true;
		}
	}
	
	public void paint(Graphics mainGraphic) 
	{
		for(int tileNumber = 0;tileNumber < listOfTiles.size(); tileNumber++)
		{
			BaseTile baseTile = listOfTiles.get(tileNumber);
			baseTile.paint(mainGraphic);
		}
	}
	
	public LevelPanel deepCopy()
	{
		return new LevelPanel(this);
	}
	
	public ArrayList<BaseTile> getListOfTiles()
	{
		return listOfTiles;
	}
	
	public Rectangle getBoundingBoxArea()
	{
		return boundingBoxArea;
	}
	
	public int getWidthInTiles()
	{
		return width;
	}
	
	public int getHeightInTiles()
	{
		return height;
	}
	
	public boolean isDoneMoving()
	{
		return doneMoving;
	}
	
	public Point getDestination()
	{
		return destination;
	}
}

package hocusPocusTesterPackage;

import hocusPocusTesterPackage.Tiles.BaseTile;
import hocusPocusTesterPackage.Tiles.TilePlayer;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

//The Level class is responsible for the game logic and graphics.
//It runs the games logic update loop and houses the JPanel that the
//games graphics are drawn on to.
public class Level extends JPanel implements KeyListener, MouseListener
{
	private static final long serialVersionUID = 1L;
	
	//The current Player that is navigating through the game.
	private Player player;
	//The destination of the player.
	private Point playerDestination;
	//The spawn of the player.
	private Point playerSpawn;
	//The path for the player.
	private ArrayList<BaseTile> path;
	//The previous tile that the Player was on.  Only changes on movement.
	private TilePlayer previousPlayerTile;
	
	//The point at which the mouse was clicked.
	private Point mousePosition;
	
	//How many panels total across.
	private int width;
	//How many panels total down.
	private int height;
	//The list of Level Panels.  Use the width to figure out the layout.
	private ArrayList<LevelPanel> listOfLevelPanels;
	//Save the level panels for resetting.
	private ArrayList<LevelPanel> savedListOfPanels;
	//Save the location of the triggers.  The first rect in each subarray is
	//the trigger and the remaining is the list of tiles that get triggered.
	private ArrayList<ArrayList<Rectangle>> savedTriggers;
	//The list of rings in order from outer to inner.
	private ArrayList<ArrayList<LevelPanel>> listOfRings;
	//The trigger system for the level.
	private TriggerHandler triggerHandler;
	
	//Is there a level to load.
	private boolean loading;
	
	//Has updating finished.
	private boolean updatingFinished;
	//Has painting finished.
	private boolean paintingFinished;
	
	//Has the mouse been pressed.
	private boolean mouseHeld;
	//Has the mouse been clicked on the player and the game moved to the Level Panel
	//rotating stage.
	private boolean levelRotation;
	private boolean doneRotating;
	
	//The time.  Used to check for the update time rate.
	private int timeElapsed;
	//The last time since the last time check.
	private int lastTimeUpdate;
	//The amount of time(milliseconds) to wait before moving to the next tile in the path.
	private static final int TIME_BETWEEN_TILE_MOVEMENT = 250;
	//The file name for the rotation vortex sprite.
	private final static String ROTATION_VORTEX_FILENAME = "Art/SpiralVortex.png";
	private final static Image ROTATION_VORTEX_SPRITE = new ImageIcon(ROTATION_VORTEX_FILENAME).getImage();
	
	//The dimensions are in level panels.
	public Level(int levelWidth, int levelHeight, int tilesInLevelPanel)
	{
		timeElapsed = 0;
		lastTimeUpdate = (int)System.currentTimeMillis();
		
		loading = false;
		mouseHeld = false;
		updatingFinished = true;
		paintingFinished = true;
		levelRotation = false;
		
		addKeyListener(this);
		addMouseListener(this);
		
		width = levelWidth;
		height = levelHeight;
		listOfLevelPanels = new ArrayList<LevelPanel>();
		savedListOfPanels = new ArrayList<LevelPanel>();
		savedTriggers = new ArrayList<ArrayList<Rectangle>>();
		listOfRings = new ArrayList<ArrayList<LevelPanel>>();
				
		triggerHandler = new TriggerHandler();
		
		path = new ArrayList<BaseTile>();
		
		mousePosition = new Point();
		
		player = new Player();
		playerSpawn = new Point(0, 0);
		playerDestination = new Point(0, 0);
		previousPlayerTile = new TilePlayer((int)(player.getBoundingBox().getLocation().getX()), (int)(player.getBoundingBox().getLocation().getY()));
	}
	
	private void reloadLevel()
	{
		if(updatingFinished && paintingFinished && loading)
		{
			//Reset the level.
			path.clear();	
			
			listOfLevelPanels.clear();
			listOfRings.clear();
			
			triggerHandler.reset();

			mouseHeld = false;
			
			levelRotation = false;
			
			//Create the objects that will handle the Player information.
			player = new Player();
			player.moveTo((int)(playerSpawn.getX()), (int)(playerSpawn.getY()));
			playerDestination.setLocation((int)(playerSpawn.getX()), (int)(playerSpawn.getY()));
			previousPlayerTile = new TilePlayer((int)(player.getBoundingBox().getLocation().getX()), (int)(player.getBoundingBox().getLocation().getY()));
			
			//Copy the saved level to the used level.
			for(int savedPanelNumber = 0 ; savedPanelNumber < savedListOfPanels.size(); savedPanelNumber++)
			{
				LevelPanel levelPanel = savedListOfPanels.get(savedPanelNumber);
				listOfLevelPanels.add(levelPanel.deepCopy());
			}
			
			//Add the triggers back.
			for(int listOfTriggerNumber = 0; listOfTriggerNumber < savedTriggers.size(); listOfTriggerNumber++)
			{
				BaseTile triggerTile = getTileAt(savedTriggers.get(listOfTriggerNumber).get(0).getLocation());
				
				ArrayList<BaseTile> triggeredTiles = new ArrayList<BaseTile>();
				
				for(int triggeredTileNumber = 0; triggeredTileNumber < savedTriggers.get(listOfTriggerNumber).size(); triggeredTileNumber++)
				{
					triggeredTiles.add(getTileAt(savedTriggers.get(listOfTriggerNumber).get(triggeredTileNumber).getLocation()));
				}
				
				triggerHandler.addNewTrigger(triggerTile, triggeredTiles);
			}
			
			//Find and fill the rings.
			//Fill in the array with place holders.
			for(int ringPlaceholder = 0; ringPlaceholder < Math.floor(width/2); ringPlaceholder++)
			{
				listOfRings.add(new ArrayList<LevelPanel>());
			}
			findRings();
			
			//Add the planes to the level.  Must add in left to right and up and down order.
			loading = false;
			updatingFinished = false;
			paintingFinished = false;
		}
	}
	
	public void resetLevel()
	{
		loading = true;
		mouseHeld = false;
		//See if the level needs to be reloaded.  This is here to stop
		//the concurrent modification error.  Will wait until it is safe
		//to reload the level.
		while(!updatingFinished || !paintingFinished);
		reloadLevel();
	}
	
	 //Add a notifier and request focus to make keys and mouse work.
	 public void addNotify() 
	 {
		 super.addNotify();
	     requestFocus();
	 }
	
	//Adds/Replaces a Level Plane to the right place.
	public void addLevelPanel(LevelPanel newLevelPanel)
	{
		listOfLevelPanels.add(newLevelPanel);
		savedListOfPanels.add(newLevelPanel.deepCopy());
	}
	
	//Update the game's objects and run the game's logic.
	public void update()
	{
		//Has the player moved this update.
		boolean moved = false;
		updatingFinished = false;
		if(!loading)
		{
			doneRotating = true;
			
			for(int panelNumber = 0;panelNumber < listOfLevelPanels.size(); panelNumber++)
			{
				if(!listOfLevelPanels.get(panelNumber).isDoneMoving())
				{
					doneRotating = false;
				}
			}

			timeElapsed += System.currentTimeMillis() - lastTimeUpdate;
			lastTimeUpdate = (int) System.currentTimeMillis();
			
			if(mouseHeld && doneRotating && !levelRotation)
			{
				if(path.isEmpty())
				{
					//Note:This is assuming that all level panels are the same sizes.
					PathFinding newPathFinder = new PathFinding(GlobalConstants.getLevelWidth()/GlobalConstants.getTileSize(), GlobalConstants.getLevelHeight()/GlobalConstants.getTileSize(), listOfLevelPanels);
					path = newPathFinder.findPath(new TilePlayer((int)(player.getBoundingBox().getX()), (int)(player.getBoundingBox().getY())), new TilePlayer((int)(mousePosition.getX()), (int)(mousePosition.getY())));
				}
	
				if(!path.isEmpty() && timeElapsed >= TIME_BETWEEN_TILE_MOVEMENT && !loading)
				{
					moved = true;
					//Note:Calculation here depends on panels having the same sizes.  Using
					//the first panel in the array list.
					previousPlayerTile.moveTo((int)(player.getBoundingBox().getX()), (int)(player.getBoundingBox().getY()));
					player.moveTo((int)(path.get(path.size() - 1).getBoundingBox().getX()), (int)(path.get(path.size() - 1).getBoundingBox().getY()));
					playerDestination.move((int)player.getBoundingBox().getX(), (int)player.getBoundingBox().getY());
					
					path.remove(path.size() - 1);
					timeElapsed = 0;
				}
			}
			
			if(levelRotation && mouseHeld)
			{				
				//Get the bounding box to use for the dimension.
				Rectangle levelPanelBoundingBox = listOfLevelPanels.get(0).getBoundingBoxArea();
				
				//Create four rectangles that would cut the level into four areas.  The mouse movement will be
				//interpreted by which area the mouse is in.
				Rectangle topEdge = new Rectangle(0, 0, (int)(width*levelPanelBoundingBox.getWidth()), (int)(levelPanelBoundingBox.getHeight()*Math.floor(width/2)));
				Rectangle bottomEdge = new Rectangle(0, (int)((height * levelPanelBoundingBox.getHeight()) - (levelPanelBoundingBox.getHeight()*Math.floor(width/2))), (int)(width*levelPanelBoundingBox.getWidth()), (int)(levelPanelBoundingBox.getHeight()*Math.floor(width/2)));
				Rectangle rightEdge = new Rectangle(0, 0, (int)(levelPanelBoundingBox.getWidth()*Math.floor(width/2)), (int)(height * levelPanelBoundingBox.getHeight()));
				Rectangle leftEdge = new Rectangle((int)((width * levelPanelBoundingBox.getWidth()) - (levelPanelBoundingBox.getWidth()*Math.floor(width/2))), 0, (int)(levelPanelBoundingBox.getWidth()*Math.floor(width/2)), (int)(height * levelPanelBoundingBox.getHeight()));
				
				//Track the mouse now and compare it to where it was on the last rotation/click.
				Point mouseLocationNow = MouseInfo.getPointerInfo().getLocation();
				SwingUtilities.convertPointFromScreen(mouseLocationNow, this);
				
				//Calculate the mouse difference.
				Point mouseDifference = new Point (mouseLocationNow.x - mousePosition.x, mouseLocationNow.y - mousePosition.y);
				
				//Change the rotation if the threshold has been met and the mouse started in the valid area.
				if(topEdge.contains(mousePosition) || bottomEdge.contains(mousePosition))
				{
					if(mouseDifference.x >= levelPanelBoundingBox.getWidth())
					{
						if(topEdge.contains(mousePosition))
						{
							rotateRing(0, true);
							mousePosition = mouseLocationNow;
						}
						else
						{
							rotateRing(0, false);
							mousePosition = mouseLocationNow;
						}
					}
					else if(mouseDifference.x <= -levelPanelBoundingBox.getWidth())
					{
						if(topEdge.contains(mousePosition))
						{
							rotateRing(0, false);
							mousePosition = mouseLocationNow;
						}
						else
						{
							rotateRing(0, true);
							mousePosition = mouseLocationNow;
						}
					}
				}
				if(rightEdge.contains(mousePosition) || leftEdge.contains(mousePosition))
				{
					if(mouseDifference.y <= -levelPanelBoundingBox.getHeight())
					{
						if(rightEdge.contains(mousePosition))
						{
							rotateRing(0, true);
							mousePosition = mouseLocationNow;
						}
						else
						{
							rotateRing(0, false);
							mousePosition = mouseLocationNow;
						}
					}
					else if(mouseDifference.y >= levelPanelBoundingBox.getHeight())
					{
						if(rightEdge.contains(mousePosition))
						{
							rotateRing(0, false);
							mousePosition = mouseLocationNow;
						}
						else
						{
							rotateRing(0, true);
							mousePosition = mouseLocationNow;
						}
					}
				}
			}
			
			for(int panelNumber = 0; panelNumber < listOfLevelPanels.size(); panelNumber++)
			{
				LevelPanel levelPanel = listOfLevelPanels.get(panelNumber);
				levelPanel.update();
			}
			if(doneRotating)
			{
				triggerHandler.triggerTest(new TilePlayer((int)(player.getBoundingBox().getX()), (int)(player.getBoundingBox().getY())), this);
			}	
			player.update();
		}
		updatingFinished = true;
		if(moved)
		{
			activateOnExitTile(previousPlayerTile);
			activateOnEnterTile(new TilePlayer((int)(player.getBoundingBox().getX()), (int)(player.getBoundingBox().getY())));
		}
		if(!player.getBoundingBox().getLocation().equals(playerDestination) && doneRotating)
		{
			player.moveTo((int)(playerDestination.getX()), (int)(playerDestination.getY()));
		}
	}
	
	public void paint(Graphics mainGraphic)
	{
		paintingFinished = false;
		
		Graphics2D levelGraphic = (Graphics2D)mainGraphic;
		levelGraphic.setColor(Color.BLACK);
		levelGraphic.fillRect(0, 0, GlobalConstants.getLevelWidth(), GlobalConstants.getLevelHeight());
		
		if(!loading)
		{
			for(int panelNumber = 0; panelNumber < listOfLevelPanels.size(); panelNumber++)
			{
				LevelPanel levelPanel = listOfLevelPanels.get(panelNumber);
				levelPanel.paint(mainGraphic);
			}
			
			if(doneRotating)
			{
				player.paint(mainGraphic);
			}
			
	        if(levelRotation)
	        {
	        	//Draw the rotation vortex in the middle of the screen.
	        	//Note:This is assuming that all level panels are the same sizes.
	        	levelGraphic.drawImage(ROTATION_VORTEX_SPRITE, (width*GlobalConstants.getTileSize()*listOfLevelPanels.get(0).getWidthInTiles())/2 - GlobalConstants.getTileSize()/2, (height*GlobalConstants.getTileSize()*listOfLevelPanels.get(0).getHeightInTiles())/2 - GlobalConstants.getTileSize()/2, null);
	        }
		}
		paintingFinished = true;
    }

	//Find the tile that was left and activate it.(Might need to be redone.)
	public void activateOnExitTile(TilePlayer tempPlayerTile)
	{
		if(doneRotating)
		{
			for(int panelNumber = 0; panelNumber < listOfLevelPanels.size(); panelNumber++)
			{
				LevelPanel levelPanel = listOfLevelPanels.get(panelNumber);
				if(levelPanel.getBoundingBoxArea().contains(tempPlayerTile.getBoundingBox()))
				{
					for(int tileNumber = 0;tileNumber < levelPanel.getListOfTiles().size(); tileNumber++)
					{
						BaseTile baseTile = levelPanel.getListOfTiles().get(tileNumber);
						if(baseTile.compare(tempPlayerTile) == 0)
						{
							baseTile.onExit(this);
						}
					}
				}
			}
		}
	}
	
	//Find the tile that was entered and activate it.(Might need to be redone.)
	public void activateOnEnterTile(TilePlayer tempPlayerTile)
	{
		if(doneRotating)
		{
			for(int panelNumber = 0; panelNumber < listOfLevelPanels.size(); panelNumber++)
			{
				LevelPanel levelPanel = listOfLevelPanels.get(panelNumber);
				if(levelPanel.getBoundingBoxArea().contains(tempPlayerTile.getBoundingBox()))
				{
					for(int tileNumber = 0;tileNumber < levelPanel.getListOfTiles().size(); tileNumber++)
					{
						BaseTile baseTile = levelPanel.getListOfTiles().get(tileNumber);
						if(baseTile.compare(tempPlayerTile) == 0)
						{
							baseTile.onEnter(this);
						}
					}
				}
			}
		}
	}
	
	//Process the last held key here.  No other keys will be processed
	//until there is a update where all keys are up.
	public void keyPressed(KeyEvent key) 
	{
		if(!levelRotation)
		{
			//Create the a temporary tile to see if where the Player wants to go is valid.
			TilePlayer tempPlayerTile = new TilePlayer(99999, 99999);
			boolean doneWalking = false;
			//Attempt to move up.
			if(key.getKeyCode() == KeyEvent.VK_UP)
			{
				//Create a temporary Tile Player to see if this tile is valid.
				tempPlayerTile = new TilePlayer((int)(player.getBoundingBox().getX()), (int)(player.getBoundingBox().getY() - GlobalConstants.getTileSize()));
			}
			//Attempt to move down.
			else if(key.getKeyCode() == KeyEvent.VK_DOWN)
			{
				tempPlayerTile = new TilePlayer((int)(player.getBoundingBox().getX()), (int)(player.getBoundingBox().getY() + GlobalConstants.getTileSize()));
			}
			//Attempt to move left.
			else if(key.getKeyCode() == KeyEvent.VK_LEFT)
			{
				tempPlayerTile = new TilePlayer((int)(player.getBoundingBox().getX() - GlobalConstants.getTileSize()), (int)(player.getBoundingBox().getY()));
			}
			//Attempt to move right.
			else if(key.getKeyCode() == KeyEvent.VK_RIGHT)
			{
				tempPlayerTile = new TilePlayer((int)(player.getBoundingBox().getX() + GlobalConstants.getTileSize()), (int)(player.getBoundingBox().getY()));
			}
			
			for(int currentLevelPanel = 0;currentLevelPanel < listOfLevelPanels.size(); currentLevelPanel++)
			{
				if(listOfLevelPanels.get(currentLevelPanel).getBoundingBoxArea().contains(tempPlayerTile.getBoundingBox()))
				{
					for(int currentTile = 0; currentTile < listOfLevelPanels.get(currentLevelPanel).getListOfTiles().size(); currentTile++)
					{
						if(listOfLevelPanels.get(currentLevelPanel).getListOfTiles().get(currentTile).compare(tempPlayerTile) == 0)
						{
							if(listOfLevelPanels.get(currentLevelPanel).getListOfTiles().get(currentTile).isWalkable() && !doneWalking)
							{
								doneWalking = true;
								previousPlayerTile.moveTo((int)(player.getBoundingBox().getX()), (int)(player.getBoundingBox().getY()));
								player.moveTo((int)(tempPlayerTile.getBoundingBox().getX()), (int)(tempPlayerTile.getBoundingBox().getY()));
								playerDestination.move((int)player.getBoundingBox().getX(), (int)player.getBoundingBox().getY());
							}
						}
					}
				}
			}
			
			if(doneWalking)
			{
				activateOnExitTile(previousPlayerTile);
				activateOnEnterTile(tempPlayerTile);
			}
			
			if(key.getKeyCode() == KeyEvent.VK_Z)
			{
				rotateRing(0, false);
			}
			else if(key.getKeyCode() == KeyEvent.VK_X)
			{
				rotateRing(0, true);
			}
			
			if(key.getKeyCode() == KeyEvent.VK_R)
			{
				resetLevel();
			}
		}
	}
	
	//Finds the rings and fill the list of rings so that we always have them.
	private void findRings()
	{
		//Create rectangels outlines that go along each ring possible in this level.
		//This list contains the outlines represented by four rectangles.  The list
		//does from outer ring to inner ring.
		ArrayList<ArrayList<Rectangle>> listOfOutlines = new ArrayList<ArrayList<Rectangle>>();
		
		//Grab an example level panel to get the dimensions.
		Rectangle levelPanelBoundingBox = listOfLevelPanels.get(0).getBoundingBoxArea();
		
		//Error check offset.  This is to make sure the outline only intersects the right level panels.
		int errorCheckOffset = 5;
		
		//A level can have floor(dimension/2) rings so make the rectangles
		//to represent that.
		for(int currentRingOutlineBeingMade = 0; currentRingOutlineBeingMade < Math.floor(width/2); currentRingOutlineBeingMade++)
		{
			//System.out.println("Ring # : " + currentRingOutlineBeingMade);
			Rectangle topEdge = new Rectangle((int)(currentRingOutlineBeingMade*levelPanelBoundingBox.getWidth() + errorCheckOffset), (int)(currentRingOutlineBeingMade*levelPanelBoundingBox.getHeight() + errorCheckOffset), (int)(GlobalConstants.getLevelWidth() - (levelPanelBoundingBox.getWidth()*(currentRingOutlineBeingMade + 1*currentRingOutlineBeingMade)) - (errorCheckOffset*2)), 1);
			Rectangle bottomEdge = new Rectangle((int)(currentRingOutlineBeingMade*levelPanelBoundingBox.getWidth() + errorCheckOffset), (int)(GlobalConstants.getLevelHeight() - (levelPanelBoundingBox.getBounds().getHeight() * (currentRingOutlineBeingMade)) - (errorCheckOffset * 2)), (int)(GlobalConstants.getLevelWidth() - (levelPanelBoundingBox.getWidth()*(currentRingOutlineBeingMade + 1*currentRingOutlineBeingMade)) - (errorCheckOffset*2)), 1);
			Rectangle leftEdge = new Rectangle((int)(currentRingOutlineBeingMade*levelPanelBoundingBox.getWidth() + errorCheckOffset), (int)(currentRingOutlineBeingMade*levelPanelBoundingBox.getHeight() + errorCheckOffset), 1, (int)(GlobalConstants.getLevelHeight() - (levelPanelBoundingBox.getHeight()*(currentRingOutlineBeingMade + 1*currentRingOutlineBeingMade)) - (errorCheckOffset*2)));
			Rectangle rightEdge = new Rectangle((int)(GlobalConstants.getLevelWidth() - currentRingOutlineBeingMade * levelPanelBoundingBox.getWidth() - errorCheckOffset), (int)(currentRingOutlineBeingMade*levelPanelBoundingBox.getHeight() + errorCheckOffset), 1, (int)(GlobalConstants.getLevelHeight() - (levelPanelBoundingBox.getHeight()*(currentRingOutlineBeingMade + 1*currentRingOutlineBeingMade)) - (errorCheckOffset*2)));
			
			ArrayList<Rectangle> currentRingOutline = new ArrayList<Rectangle>();
			currentRingOutline.add(topEdge);
			currentRingOutline.add(bottomEdge);
			currentRingOutline.add(leftEdge);
			currentRingOutline.add(rightEdge);
			
			listOfOutlines.add(currentRingOutline);
			
			/*for(Rectangle rectOfCurrentRing : currentRingOutline)
			{
				System.out.println("	X : " + rectOfCurrentRing.getX());
				System.out.println("	Y : " + rectOfCurrentRing.getY());
				System.out.println("	W : " + rectOfCurrentRing.getWidth());
				System.out.println("	H : " + rectOfCurrentRing.getHeight());
			}*/
		}
		
		//Go through all the panels and check to see which ring they belong to.
		
		for(int outlineNumber = 0; outlineNumber < listOfOutlines.size(); outlineNumber++)
		{
			//System.out.println("Outline # : " + outlineNumber);
			int panelNumber = 0;
			for(int levelPanelNumber = 0; levelPanelNumber < listOfLevelPanels.size(); levelPanelNumber++)
			{
				LevelPanel levelPanel = listOfLevelPanels.get(levelPanelNumber);
				for(int rectNumber = 0;rectNumber < listOfOutlines.get(outlineNumber).size(); rectNumber++)
				{
					Rectangle outlineEdge = listOfOutlines.get(outlineNumber).get(rectNumber);
					//If the panel intersects the outline then it is part of that ring.
					if(levelPanel.getBoundingBoxArea().intersects(outlineEdge))
					{
						//Do not add if it has already been added to that level panel.
						boolean levelPanelAlreadyAdded = false;
						for(int ringPanelNumber = 0; ringPanelNumber < listOfRings.get(outlineNumber).size(); ringPanelNumber++)
						{
							LevelPanel levelPanelInRing = listOfRings.get(outlineNumber).get(ringPanelNumber);
							if(levelPanelInRing.getBoundingBoxArea().getX() == levelPanel.getBoundingBoxArea().getX() && levelPanelInRing.getBoundingBoxArea().getY() == levelPanel.getBoundingBoxArea().getY())
							{
								levelPanelAlreadyAdded = true;
							}
						}
						//Add it if it is new.
						if(!levelPanelAlreadyAdded)
						{
							//System.out.println("Panel added : " + panelNumber);
							listOfRings.get(outlineNumber).add(levelPanel);
						}
					}
				}
				panelNumber++;
			}
		}
	}
	
	//Calls and rotates the ring set and in the direction set.  0 for
	//ring number means all possible rings are rotated.
	private void rotateRing(int ringNumber, boolean typeOfRotation)
	{
		boolean okToRotate = true;
		for(int panelNumber = 0; panelNumber < listOfLevelPanels.size(); panelNumber++)
		{
			if(!listOfLevelPanels.get(panelNumber).isDoneMoving())
			{
				okToRotate = false;
			}
		}
		if(okToRotate)
		{
			//Go through the array of rings and rotate them all.
			if(ringNumber == 0)
			{
				for(int ringNum = 0; ringNum < listOfRings.size(); ringNum++)
				{
					ArrayList<LevelPanel> ring = listOfRings.get(ringNum);
					ringNum++;
					rotate(ring, ringNum, typeOfRotation);
				}
			}
			//Rotate the single ring.
			else
			{
				rotate(listOfRings.get(ringNumber - 1), ringNumber, typeOfRotation);
			}
		}
	}
	
	//Rotates the board.  False is CCW and true is CW.
	//The array holds the panel of the ring that is given to this method.
	//The ring number is the count of rings.  Ring 1 is the most outer and
	//the number increases as the depth does.
	//Kind of messy.  Will have to find a simple equation to do this but for
	//now this will do.
	private void rotate(ArrayList<LevelPanel> listOfLevelPanelsToRotate, int ringNumber, boolean typeOfRotation)
	{
		//Rotate Level Planes CW.
		if(typeOfRotation)
		{
			//Make sure the player only moved once.
			boolean playerMoved = false;
			for(int panelNumber = 0; panelNumber < listOfLevelPanelsToRotate.size(); panelNumber++)
			{
				LevelPanel levelPanel = listOfLevelPanelsToRotate.get(panelNumber);
				//Keep the old bounding box of the level to figure out where to move the player.
				Rectangle oldLevelPanelBox = (Rectangle) levelPanel.getBoundingBoxArea().clone();
				
				//If the Level Panel is not at the left/right edges and is the top edge of the ring 
				//then move it to the right.
				if(levelPanel.getBoundingBoxArea().getX() != (ringNumber - 1) * levelPanel.getBoundingBoxArea().getWidth() && levelPanel.getBoundingBoxArea().getX() != GlobalConstants.getLevelWidth() - levelPanel.getBoundingBoxArea().getWidth() * ringNumber && levelPanel.getBoundingBoxArea().getY() == (ringNumber - 1) * levelPanel.getBoundingBoxArea().getHeight())
				{
					levelPanel.moveTo((int)(levelPanel.getBoundingBoxArea().getX() + levelPanel.getBoundingBoxArea().getWidth()), (int)(levelPanel.getBoundingBoxArea().getY()));
				}
				//If it is the bottom edge of the ring.
				else if(levelPanel.getBoundingBoxArea().getX() != (ringNumber - 1) * levelPanel.getBoundingBoxArea().getWidth() && levelPanel.getBoundingBoxArea().getX() != GlobalConstants.getLevelWidth() - levelPanel.getBoundingBoxArea().getWidth() * ringNumber)
				{
					//Move it to the left.
					levelPanel.moveTo((int)(levelPanel.getBoundingBoxArea().getX() - levelPanel.getBoundingBoxArea().getWidth()), (int)(levelPanel.getBoundingBoxArea().getY()));
				}
				//If they are on the right edge. 
				else if(levelPanel.getBoundingBoxArea().getX() == GlobalConstants.getLevelWidth() - levelPanel.getBoundingBoxArea().getWidth() * ringNumber)
				{
					//If the panel is at the bottom right corner.
					if(levelPanel.getBoundingBoxArea().getY() == GlobalConstants.getLevelHeight() - levelPanel.getBoundingBoxArea().getHeight() * ringNumber)
					{
						//Move it to the left.
						levelPanel.moveTo((int)(levelPanel.getBoundingBoxArea().getX() - levelPanel.getBoundingBoxArea().getWidth()), (int)(levelPanel.getBoundingBoxArea().getY()));
					}
					//If not.
					else
					{
						//Move it down.
						levelPanel.moveTo((int)(levelPanel.getBoundingBoxArea().getX()), (int)(levelPanel.getBoundingBoxArea().getY() + levelPanel.getBoundingBoxArea().getHeight()));
					}
				}
				//If they are on the left edge.
				else if(levelPanel.getBoundingBoxArea().getX() == (ringNumber - 1) * levelPanel.getBoundingBoxArea().getWidth())
				{
					//If the panel is at the top right corner.
					if(levelPanel.getBoundingBoxArea().getY() == (ringNumber - 1) * levelPanel.getBoundingBoxArea().getHeight())
					{
						//Move it to the right.
						levelPanel.moveTo((int)(levelPanel.getBoundingBoxArea().getX() + levelPanel.getBoundingBoxArea().getWidth()), (int)(levelPanel.getBoundingBoxArea().getY()));
					}
					//If not.
					else
					{
						//Move it up.
						levelPanel.moveTo((int)(levelPanel.getBoundingBoxArea().getX()), (int)(levelPanel.getBoundingBoxArea().getY() - levelPanel.getBoundingBoxArea().getHeight()));
					}
				}
				//If the player is on this level panel.  Move it the same amount as the level panel.
				if(oldLevelPanelBox.contains(player.getBoundingBox()) && !playerMoved)
				{
					playerMoved = true;
					playerDestination = new Point((int)(player.getBoundingBox().getX() + (levelPanel.getDestination().getX() - oldLevelPanelBox.getX())), (int)(player.getBoundingBox().getY() + (levelPanel.getDestination().getY() - oldLevelPanelBox.getY())));
				}
			}
		}
		//Rotate Level Planes CCW.
		else
		{
			boolean playerMoved = false;
			for(int panelNumber = 0;panelNumber < listOfLevelPanelsToRotate.size(); panelNumber++)
			{
				LevelPanel levelPanel = listOfLevelPanelsToRotate.get(panelNumber);
				//Keep the old bounding box of the level to figure out where to move the player.
				Rectangle oldLevelPanelBox = (Rectangle) levelPanel.getBoundingBoxArea().clone();
				
				//If the Level Panel is not at the left/right edges and is the top edge of the ring 
				//then move it to the left.
				if(levelPanel.getBoundingBoxArea().getX() != (ringNumber - 1) * levelPanel.getBoundingBoxArea().getWidth() && levelPanel.getBoundingBoxArea().getX() != GlobalConstants.getLevelWidth() - levelPanel.getBoundingBoxArea().getWidth() * ringNumber && levelPanel.getBoundingBoxArea().getY() == (ringNumber - 1) * levelPanel.getBoundingBoxArea().getHeight())
				{
					levelPanel.moveTo((int)(levelPanel.getBoundingBoxArea().getX() - levelPanel.getBoundingBoxArea().getWidth()), (int)(levelPanel.getBoundingBoxArea().getY()));
				}
				//If it is the bottom edge of the ring.
				else if(levelPanel.getBoundingBoxArea().getX() != (ringNumber - 1) * levelPanel.getBoundingBoxArea().getWidth() && levelPanel.getBoundingBoxArea().getX() != GlobalConstants.getLevelWidth() - levelPanel.getBoundingBoxArea().getWidth() * ringNumber)
				{
					//Move it to the right.
					levelPanel.moveTo((int)(levelPanel.getBoundingBoxArea().getX() + levelPanel.getBoundingBoxArea().getWidth()), (int)(levelPanel.getBoundingBoxArea().getY()));
				}
				//If they are on the right edge. 
				else if(levelPanel.getBoundingBoxArea().getX() == GlobalConstants.getLevelWidth() - levelPanel.getBoundingBoxArea().getWidth() * ringNumber)
				{
					//If the panel is at the top right corner.
					if(levelPanel.getBoundingBoxArea().getY() == (ringNumber - 1) * levelPanel.getBoundingBoxArea().getHeight())
					{
						//Move it to the left.
						levelPanel.moveTo((int)(levelPanel.getBoundingBoxArea().getX() - levelPanel.getBoundingBoxArea().getWidth()), (int)(levelPanel.getBoundingBoxArea().getY()));
					}
					//If not.
					else
					{
						//Move it up.
						levelPanel.moveTo((int)(levelPanel.getBoundingBoxArea().getX()), (int)(levelPanel.getBoundingBoxArea().getY() - levelPanel.getBoundingBoxArea().getHeight()));
					}
				}
				//If they are on the left edge.
				else if(levelPanel.getBoundingBoxArea().getX() == (ringNumber - 1) * levelPanel.getBoundingBoxArea().getWidth())
				{
					//If the panel is at the bottom left corner.
					if(levelPanel.getBoundingBoxArea().getY() == GlobalConstants.getLevelHeight() - levelPanel.getBoundingBoxArea().getHeight() * ringNumber)
					{
						//Move it to the right.
						levelPanel.moveTo((int)(levelPanel.getBoundingBoxArea().getX() + levelPanel.getBoundingBoxArea().getWidth()), (int)(levelPanel.getBoundingBoxArea().getY()));
					}
					//If not.
					else
					{
						//Move it down.
						levelPanel.moveTo((int)(levelPanel.getBoundingBoxArea().getX()), (int)(levelPanel.getBoundingBoxArea().getY() + levelPanel.getBoundingBoxArea().getHeight()));
					}
				}
				//If the player is on this level panel.  Move it the same amount as the level panel.
				if(oldLevelPanelBox.contains(player.getBoundingBox()) && !playerMoved)
				{
					playerMoved = true;
					playerDestination = new Point((int)(player.getBoundingBox().getX() + (levelPanel.getDestination().getX() - oldLevelPanelBox.getX())), (int)(player.getBoundingBox().getY() + (levelPanel.getDestination().getY() - oldLevelPanelBox.getY())));
				}
			}
		}
	}

	//Finds the Point of the tile that is closest to the old point.
	//If the point is unreachable then return the old position.
	private Point findTile(Point oldPoint)
	{
		for(int panelNumber = 0; panelNumber < listOfLevelPanels.size(); panelNumber++)
		{
			LevelPanel levelPanel = listOfLevelPanels.get(panelNumber);
			for(int tileNumber = 0; tileNumber < levelPanel.getListOfTiles().size(); tileNumber++)
			{
				BaseTile baseTile = levelPanel.getListOfTiles().get(tileNumber);
				if(baseTile.getBoundingBox().contains(oldPoint))
				{
					return baseTile.getBoundingBox().getLocation();
				}
			}
		}
		return oldPoint;
	}
	
	//Find the actual tile in the location.
	public BaseTile getTileAt(Point location)
	{
		for(int panelNumber = 0; panelNumber < listOfLevelPanels.size(); panelNumber++)
		{
			LevelPanel levelPanel = listOfLevelPanels.get(panelNumber);
			for(int tileNumber = 0; tileNumber < levelPanel.getListOfTiles().size(); tileNumber++)
			{
				BaseTile baseTile = levelPanel.getListOfTiles().get(tileNumber);
				if(baseTile.getBoundingBox().contains(location))
				{
					return baseTile;
				}
			}
		}
		return new TilePlayer(GlobalConstants.getNotANumber(), GlobalConstants.getNotANumber());
	}
	
	public void keyReleased(KeyEvent key) 
	{
	}

	public void keyTyped(KeyEvent key) 
	{
	}

	public void mouseClicked(MouseEvent mouse)
	{	
	}

	public void mouseEntered(MouseEvent mouse) 
	{
	}

	public void mouseExited(MouseEvent mouse) 
	{	
	}

	public void mousePressed(MouseEvent mouse) 
	{
		if(!loading)
		{
			//Set the temp boolean and grab the mouse location.
			//The actual boolean is not set because this mouse listener runs
			//on a sperate thread.
			boolean mouseHeldTemp = true;
			mousePosition.setLocation(mouse.getPoint());
			//If the the game is in the level rotate state then any clicks in the
			//middle will reset the board.
			if(levelRotation && new Rectangle((width*GlobalConstants.getTileSize()*listOfLevelPanels.get(0).getWidthInTiles())/2 - GlobalConstants.getTileSize()/2 , (height*GlobalConstants.getTileSize()*listOfLevelPanels.get(0).getHeightInTiles())/2 - GlobalConstants.getTileSize()/2, GlobalConstants.getTileSize(), GlobalConstants.getTileSize()).contains(mousePosition))
			{
				levelRotation = false;
				mouseHeldTemp = false;
			}
			mousePosition = findTile(mousePosition);
			if(mousePosition.equals(player.getBoundingBox().getLocation()))
			{
				levelRotation = true;
				mouseHeldTemp = false;
				//The last found path is now invalid.
				path.clear();
			}
			mouseHeld = mouseHeldTemp;
		}
	}

	public void mouseReleased(MouseEvent mouse)
	{	
		mouseHeld = false;
		path.clear();
	}
	
	public TilePlayer getPreviousPlayerTile()
	{
		return previousPlayerTile;
	}
	
	
	public void movePlayer(int newXPosition, int newYPosition)
	{
		previousPlayerTile.moveTo((int)(player.getBoundingBox().getLocation().getX()), (int)(player.getBoundingBox().getLocation().getY()));
		player.moveTo(newXPosition, newYPosition);
		playerDestination.setLocation(newXPosition, newYPosition);
	}

	public Player getPlayer() 
	{
		return player;
	}
	
	public int getLevelDimensions()
	{
		return GlobalConstants.getTileSize() * width * listOfLevelPanels.get(0).getWidthInTiles();
	}
	
	public void createRings()
	{
		for(int ringPlaceholder = 0; ringPlaceholder < Math.floor(width/2); ringPlaceholder++)
		{
			listOfRings.add(new ArrayList<LevelPanel>());
		}
		findRings();
	}
	
	public void addTrigger(BaseTile trigger, ArrayList<BaseTile> listOfTriggeredTiles)
	{
		triggerHandler.addNewTrigger(trigger, listOfTriggeredTiles);
		
		ArrayList<Rectangle> newListOfTriggers = new ArrayList<Rectangle>();
		
		newListOfTriggers.add(trigger.getBoundingBox());
		for(int rectNumber = 0; rectNumber < listOfTriggeredTiles.size(); rectNumber++)
		{
			newListOfTriggers.add(listOfTriggeredTiles.get(rectNumber).getBoundingBox());
		}
		
		savedTriggers.add(newListOfTriggers);
	}
	
	public void setSpawn(int newXPosition, int newYPosition)
	{
		playerSpawn.setLocation(newXPosition, newYPosition);
	}
	
	public ArrayList<LevelPanel> getListOfLevelPanels()
	{
		return listOfLevelPanels;
	}
}

package hocusPocusTesterPackage;

import java.util.ArrayList;

import hocusPocusTesterPackage.Tiles.BaseTile;
import hocusPocusTesterPackage.Tiles.TileIce;
import hocusPocusTesterPackage.Tiles.TileKey;
import hocusPocusTesterPackage.Tiles.TileWall;
import hocusPocusTesterPackage.Tiles.TileWin;
import fileIO.LevelKit;
import fileIO.TileData;

//The level loader loads and returns the level from the Level Data class.
public class LevelLoader 
{
	public LevelLoader()
	{
	}
	
	public Level loadLevel(Level level, LevelKit levelKit)
	{
		fileIO.LevelData levelData = levelKit.getLevelData();
		//Create level with the row and and column of the panels.
		level = new Level(levelData.getLevelDimension(), levelData.getLevelDimension(), levelData.getSectionDimension());
		
		for(int layer = 0; layer < levelData.getNumLayers(); layer++)
		{
			TileData[][] tileArrayData = levelKit.getTileData()[layer];
			
			//Loop through all ssections.
			for(int currentSectionY = 0; currentSectionY < levelData.getLevelDimension(); currentSectionY++)
			{
				for(int currentSectionX = 0; currentSectionX < levelData.getLevelDimension(); currentSectionX++)
				{
					//Create the level panel with the right dim.
					LevelPanel newLevelPanel = new LevelPanel(currentSectionX*levelData.getSectionDimension()*GlobalConstants.getTileSize(), currentSectionY*levelData.getSectionDimension()*GlobalConstants.getTileSize(), levelData.getSectionDimension(), levelData.getSectionDimension());
					
					//Go through the tiles for this level panel.
					for(int tileY = 0; tileY < levelData.getSectionDimension(); tileY++)
					{
						for(int tileX = 0; tileX < levelData.getSectionDimension(); tileX++)
						{
							TileData tileData = tileArrayData[tileX + currentSectionX*levelData.getSectionDimension()][tileY + currentSectionY*levelData.getSectionDimension()];
							//A new level panel already has been filled with walkable tiles
							//so there is no need to do so.
							if(tileData.getType() != 'e')
							{
								//Place a new tile if it is not a walkable one.
								//Wall tile.
								if(tileData.getType() == 'a')
								{
									newLevelPanel.addTile(tileX, tileY, new TileWall());
								}
								//Start tile.
								else if(tileData.getType() == 'b')
								{
									//Place the player.
									level.movePlayer(currentSectionX*levelData.getSectionDimension()*GlobalConstants.getTileSize() + GlobalConstants.getTileSize()*tileX, currentSectionY*levelData.getSectionDimension()*GlobalConstants.getTileSize() + GlobalConstants.getTileSize()*tileY);
									level.setSpawn(currentSectionX*levelData.getSectionDimension()*GlobalConstants.getTileSize() + GlobalConstants.getTileSize()*tileX, currentSectionY*levelData.getSectionDimension()*GlobalConstants.getTileSize() + GlobalConstants.getTileSize()*tileY);
								}
								//Key tile.
								else if(tileData.getType() == 'c')
								{
									newLevelPanel.addTile(tileX, tileY, new TileKey());
								}
								//Exit tile.
								else if(tileData.getType() == 'd')
								{
									newLevelPanel.addTile(tileX, tileY, new TileWin());
								}
								//Ice tile.
								else if(tileData.getType() == 'f')
								{
									newLevelPanel.addTile(tileX, tileY, new TileIce());
								}
							}
						}
					}
					//Add the level panel.
					level.addLevelPanel(newLevelPanel);
				}
			}
		}
		//Add the key and door triggers.  Might be better to do this in the level editor with tags.
		BaseTile doorTile = null;
		BaseTile keyTile = null;
		for(int levelPanelNumber = 0; levelPanelNumber < level.getListOfLevelPanels().size(); levelPanelNumber++)
		{
			for(int panelNumber = 0; panelNumber < level.getListOfLevelPanels().get(levelPanelNumber).getListOfTiles().size(); panelNumber++)
			{
				if(level.getListOfLevelPanels().get(levelPanelNumber).getListOfTiles().get(panelNumber) instanceof TileKey)
				{
					keyTile = level.getListOfLevelPanels().get(levelPanelNumber).getListOfTiles().get(panelNumber);
				}
				else if(level.getListOfLevelPanels().get(levelPanelNumber).getListOfTiles().get(panelNumber) instanceof TileWin)
				{
					doorTile = level.getListOfLevelPanels().get(levelPanelNumber).getListOfTiles().get(panelNumber);
				}
			}
		}
		
		ArrayList<BaseTile> listOfTriggeredTiles = new ArrayList<BaseTile>();
		listOfTriggeredTiles.add(doorTile);
		level.addTrigger(keyTile, listOfTriggeredTiles);
		
		return level;
	}
}

package hocusPocusTesterPackage;

//The Global Constants class holds a list of variables that are needed
//at different parts of the game.  A unit in this class consists of a
//variable and a getter or setter or both(All units have getters but
//only modifiable objects have setters).
public class GlobalConstants 
{
	//How many seconds does the game wait until it updates
	//the game state.
	private static double CYCLE_RATE = 16.6;
	public static double getCycleRate()
	{
        return CYCLE_RATE;
    }
	
	//The width of the current level.
	private static int LEVEL_WIDTH = 0;
    public static int getLevelWidth()
    {
        return LEVEL_WIDTH;
    }
    public static void setLevelWidth(int width)
    {
        LEVEL_WIDTH = width;
    }
    
    
    //The height of the current level.
    private static int LEVEL_HEIGTH = 0;
    public static int getLevelHeight()
    {
    	return LEVEL_HEIGTH;
    }
    public static void setLevelHeight(int height)
    {
    	LEVEL_HEIGTH = height;
    }
    
    //The width of the current level.
  	private static int TILE_SIZE = 32;
  	public static int getTileSize()
  	{
  		return TILE_SIZE;
  	}
  	
  	//Not a number.
  	private static int NaN = -9999;
  	public static int getNotANumber()
  	{
  		return NaN;
  	}
}

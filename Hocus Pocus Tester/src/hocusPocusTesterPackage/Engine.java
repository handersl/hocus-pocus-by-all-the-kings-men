package hocusPocusTesterPackage;

import java.awt.Dimension;

import javax.swing.JFrame;

import fileIO.FileIO;


//The engine class is responsible for the higher level game processes.
//Its purpose is to allow the Level class to just have to take care
//of game logic and rendering.  The Engine class loads levels and takes
//care of the higher level of execution.
public class Engine extends JFrame
{
	private static final long serialVersionUID = 1L;

	//Checks if the Engine is running or not.
	private boolean running;
	
	//The level that is being tested.
	private Level currentLevel;
	
	//Keeps the frame in check.
	private static final int FRAME_OFFSET = 10;
	
	private double timeSinceLastUpdate;
	private double timeAtLastUpdate;
	
	//Creates the engine and starts it.  This will prompt the
	//updating and rendering of the game.
	public Engine()
	{
		//Load the level.
		currentLevel = new LevelLoader().loadLevel(currentLevel, FileIO.loadLevel());
		
		//Start the engine running.
		running = true;
		
		timeSinceLastUpdate = 0;
		
		//Create the board that the game will display on.
		//Create the display to hold the game based on the level.
		//Set the level dimensions and create the frame.
		GlobalConstants.setLevelWidth(currentLevel.getLevelDimensions());
		GlobalConstants.setLevelHeight(currentLevel.getLevelDimensions());
		
		//After all the variables are set find the rings for the level.
		currentLevel.createRings();
		
		createFrame();
	}
	
	//Creates the frame for the game based on the size set by the Engine.
	private void createFrame()
	{
		add(currentLevel);
        setTitle("Hocus Pocus Tester");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        getContentPane().setPreferredSize(new Dimension(GlobalConstants.getLevelWidth() - FRAME_OFFSET, GlobalConstants.getLevelHeight() - FRAME_OFFSET));
        setLocationRelativeTo(null);
        setVisible(true);
        setResizable(false);
        pack();
        
        timeAtLastUpdate = System.currentTimeMillis();
	}
	
	//Update the level.
	public void update()
	{
		timeSinceLastUpdate += System.currentTimeMillis() - timeAtLastUpdate;
		timeAtLastUpdate = System.currentTimeMillis();
		if(timeSinceLastUpdate >= GlobalConstants.getCycleRate())
		{
			timeSinceLastUpdate = 0;
			currentLevel.update();
		}
		setVisible(true);
		currentLevel.repaint();
	}

	//Is the engine still running.
	public boolean isRunning()
	{
		return running;
	}
}

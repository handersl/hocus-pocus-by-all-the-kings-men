package hocusPocusTesterPackage.Tiles;

import hocusPocusTesterPackage.GlobalConstants;
import hocusPocusTesterPackage.Level;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;

import javax.swing.ImageIcon;

//This is a Tile Win.  It ends the game.  In case of the tester it restarts the level.
public class TileWin extends BaseTile
{
	//The Win Tile art filename.
    private final static String TILE_WIN_CLOSED_SPRITE_FILENAME = "Art/Tile Win Closed.png";
    private final static String TILE_WIN_OPEN_SPRITE_FILENAME = "Art/Tile Win Open.png";
    
    //The offset to draw the open door properly.
    private final static int DOOR_OPEN_OFFSET = 15;
    
    private boolean isOpen;
    
	public TileWin()
	{
		walkable = true;
		tileSprite = new ImageIcon(TILE_WIN_CLOSED_SPRITE_FILENAME).getImage();
		tileBoundingBox = new Rectangle(0, 0, GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		triggered = false;
		isOpen = false;
	}
	
	public TileWin(int xPos, int yPos, boolean open)
	{
		walkable = false;
		if(!open)
		{
			tileSprite = new ImageIcon(TILE_WIN_CLOSED_SPRITE_FILENAME).getImage();
		}
		else
		{
			tileSprite = new ImageIcon(TILE_WIN_OPEN_SPRITE_FILENAME).getImage();
		}
		tileBoundingBox = new Rectangle(xPos, yPos, GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		triggered = false;
		isOpen = open;
	}
	
	public TileWin(TileWin oldTileWin)
	{
		this.walkable = oldTileWin.isWalkable();
		if(!isOpen)
		{
			tileSprite = new ImageIcon(TILE_WIN_CLOSED_SPRITE_FILENAME).getImage();
		}
		else
		{
			tileSprite = new ImageIcon(TILE_WIN_OPEN_SPRITE_FILENAME).getImage();
		}
		this.tileBoundingBox = new Rectangle((int)(oldTileWin.getBoundingBox().getX()), (int)(oldTileWin.getBoundingBox().getY()), GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		this.triggered = oldTileWin.triggered;
		this.isOpen = oldTileWin.isOpen;
	}
	
	public void update()
	{
	}

	public void onEnter(Level level) 
	{
		if(isOpen)
		{
			level.resetLevel();
		}
	}

	public void onExit(Level level)
	{
	}

	public void onTriggerEnter(Level level) 
	{
		isOpen = true;
		tileSprite = new ImageIcon(TILE_WIN_OPEN_SPRITE_FILENAME).getImage();
	}
	
	//Override the paint as well.
	public void paint(Graphics mainGraphic) 
	{
		Graphics2D tileGraphic = (Graphics2D)mainGraphic;
		if(!isOpen)
		{
			tileGraphic.drawImage(tileSprite, tileBoundingBox.x, tileBoundingBox.y, null);
		}
		else
		{
			tileGraphic.drawImage(tileSprite, tileBoundingBox.x - DOOR_OPEN_OFFSET, tileBoundingBox.y, null);
		}
	}

	public BaseTile deepCopy() 
	{
		return new TileWin(this);
	}
}

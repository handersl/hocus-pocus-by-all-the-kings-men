package hocusPocusTesterPackage.Tiles;

import hocusPocusTesterPackage.GlobalConstants;
import hocusPocusTesterPackage.Level;

import java.awt.Rectangle;

import javax.swing.ImageIcon;

//A Tile Walkable is a tile that can be walked into.
public class TileWalkable extends BaseTile
{	
	//The Walkable Tile art filename.
    private final static String TILE_WALKABLE_SPRITE_FILENAME = "Art/Tile Walkable.png";
	
	//Create a Tile Walkable at (0,0).
	public TileWalkable()
	{
		walkable = true;
		tileSprite = new ImageIcon(TILE_WALKABLE_SPRITE_FILENAME).getImage();
		tileBoundingBox = new Rectangle(0, 0, GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		triggered = false;
	}
	
	//Create a Tile Unwalkable at (xPos, yPos).
	public TileWalkable(int xPos, int yPos)
	{
		walkable = true;
		tileSprite = new ImageIcon(TILE_WALKABLE_SPRITE_FILENAME).getImage();
		tileBoundingBox = new Rectangle(xPos, yPos, GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		triggered = false;
	}
	
	public TileWalkable(TileWalkable oldTileWalkable)
	{
		walkable = true;
		tileSprite = new ImageIcon(TILE_WALKABLE_SPRITE_FILENAME).getImage();
		tileBoundingBox = new Rectangle((int)(oldTileWalkable.getBoundingBox().getX()), (int)(oldTileWalkable.getBoundingBox().getY()), GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		triggered = false;
	}
	
	public void update() 
	{
		//Do nothing.	
	}
	
	public void onEnter(Level level)
	{
		//Do Nothing.
	}

	public void onExit(Level level) 
	{
		//Do Nothing.
	}

	public void onTriggerEnter(Level level) 
	{
	}

	@Override
	public BaseTile deepCopy() 
	{
		return new TileWalkable(this);
	}
}

package hocusPocusTesterPackage.Tiles;

import hocusPocusTesterPackage.GlobalConstants;
import hocusPocusTesterPackage.Level;

import java.awt.Rectangle;

import javax.swing.ImageIcon;

//A Tile Wall is a tile that can not be walked into.
public class TileWall extends BaseTile
{	
	//The Wall Tile art filename.
    private final static String TILE_WALL_SPRITE_FILENAME = "Art/Tile Wall.png";
	
	public TileWall()
	{
		walkable = false;
		tileSprite = new ImageIcon(TILE_WALL_SPRITE_FILENAME).getImage();
		tileBoundingBox = new Rectangle(0, 0, GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		triggered = false;
	}
	
	public TileWall(int xPos, int yPos)
	{
		walkable = false;
		tileSprite = new ImageIcon(TILE_WALL_SPRITE_FILENAME).getImage();
		tileBoundingBox = new Rectangle(xPos, yPos, GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		triggered = false;
	}
	
	public TileWall(TileWall oldTileWall)
	{
		walkable = false;
		tileSprite = new ImageIcon(TILE_WALL_SPRITE_FILENAME).getImage();
		tileBoundingBox = new Rectangle((int)(oldTileWall.getBoundingBox().getX()), (int)(oldTileWall.getBoundingBox().getY()), GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		triggered = false;
	}
	
	public void update()
	{
	}
	
	public void onEnter(Level level) 
	{
	}

	public void onExit(Level level) 
	{
	}

	public void onTriggerEnter(Level level) 
	{
	}

	public BaseTile deepCopy() 
	{
		return new TileWall(this);
	}
}

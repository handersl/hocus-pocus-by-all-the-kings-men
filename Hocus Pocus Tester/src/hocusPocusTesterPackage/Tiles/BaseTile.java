package hocusPocusTesterPackage.Tiles;

import hocusPocusTesterPackage.GlobalConstants;
import hocusPocusTesterPackage.Level;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;

//The BaseTile is the tile with no functionality but
//shapes the tiles that extend it.
public abstract class BaseTile
{	
	protected boolean walkable;
	protected Rectangle tileBoundingBox;
	protected Image tileSprite;
	protected boolean triggered;
	
	//The update logic loop for the tile.
	abstract public void update();
	//Executes when the Player enters the tile.
	abstract public void onEnter(Level level);
	//Executes when the Player leaves the tile.
	abstract public void onExit(Level level);
	//Executes when the Player activates this tile's trigger.
	abstract public void onTriggerEnter(Level level);
	//Return a deep copy of the tile.
	abstract public BaseTile deepCopy();
	
	public void moveTo(int xPos, int yPos) 
	{
		tileBoundingBox = new Rectangle(xPos, yPos, GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
	}
	
	public void paint(Graphics mainGraphic) 
	{
		Graphics2D tileGraphic = (Graphics2D)mainGraphic;
		tileGraphic.drawImage(tileSprite, tileBoundingBox.x, tileBoundingBox.y, null);
	}
	
	public Rectangle getBoundingBox()
	{
		return tileBoundingBox;
	}
	
	public boolean isWalkable() 
	{
		return walkable;
	}
	
	//Compares tiles.  Tiles will never be placed on top of each other so
	//all this will do is compare the locations of the tiles.  Tiles that
	//are closer to the bottom right are bigger.  It goes tileOne - tileTwo.
	//Right now it is overloaded to allow the equals method to work; it does
	//not work for comparisons yet.
	public int compare(BaseTile tiletwo)
	{
		if(this.getBoundingBox().getX() == tiletwo.getBoundingBox().getX() && this.getBoundingBox().getY() == tiletwo.getBoundingBox().getY())
		{
			return 0;
		}
		return -1;
	}
}

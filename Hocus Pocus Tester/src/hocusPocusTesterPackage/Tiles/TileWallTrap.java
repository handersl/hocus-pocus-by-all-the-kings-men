package hocusPocusTesterPackage.Tiles;

import hocusPocusTesterPackage.GlobalConstants;
import hocusPocusTesterPackage.Level;

import java.awt.Rectangle;

import javax.swing.ImageIcon;

//Tile Wall Traps are tiles that can only be walked over once.  Once they are
//passed they become walls tiles and can not be walked over again.
public class TileWallTrap extends BaseTile
{
	//The Wall Trap Tile art filename.
    private final static String TILE_WALL_TRAP_SPRITE_FILENAME = "Art/Tile Wall Trap.png";
    //The Wall Tile art filename.
    private final static String TILE_WALL_SPRITE_FILENAME = "Art/Tile Wall.png";
    
	public TileWallTrap()
	{
		walkable = true;
		tileSprite = new ImageIcon(TILE_WALL_TRAP_SPRITE_FILENAME).getImage();
		tileBoundingBox = new Rectangle(0, 0, GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		triggered = false;
	}
	
	public TileWallTrap(int xPos, int yPos)
	{
		walkable = true;
		tileSprite = new ImageIcon(TILE_WALL_TRAP_SPRITE_FILENAME).getImage();
		tileBoundingBox = new Rectangle(xPos, yPos, GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		triggered = false;
	}
	
	public TileWallTrap(TileWallTrap oldTileWallTrap)
	{
		walkable = oldTileWallTrap.isWalkable();
		tileSprite = new ImageIcon(TILE_WALL_TRAP_SPRITE_FILENAME).getImage();
		tileBoundingBox = new Rectangle((int)(oldTileWallTrap.getBoundingBox().getX()), (int)(oldTileWallTrap.getBoundingBox().getY()), GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		triggered = false;
	}

	public void update() 
	{
	}

	public void onEnter(Level level)
	{
	}

	public void onExit(Level level)
	{
		triggered = false;
		walkable = false;
		tileSprite = new ImageIcon(TILE_WALL_SPRITE_FILENAME).getImage();
	}

	public void onTriggerEnter(Level level) 
	{
		triggered = true;
		onExit(level);
	}

	public BaseTile deepCopy() 
	{
		return new TileWallTrap(this);
	}
}

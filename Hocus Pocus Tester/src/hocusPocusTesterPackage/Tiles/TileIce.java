package hocusPocusTesterPackage.Tiles;

import java.awt.Point;
import java.awt.Rectangle;

import javax.swing.ImageIcon;

import hocusPocusTesterPackage.GlobalConstants;
import hocusPocusTesterPackage.Level;

//The ice tile forces the player to exit it in the same direction it was entered.
public class TileIce extends BaseTile
{
	private final static String TILE_ICE_SPRITE_FILENAME = "Art/Tile Ice.png";
	
	public TileIce()
	{
		walkable = true;
		tileBoundingBox = new Rectangle(0, 0, GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		tileSprite = new ImageIcon(TILE_ICE_SPRITE_FILENAME).getImage();
		triggered = false;
	}
	
	public TileIce(int xPos, int yPos)
	{
		walkable = true;
		tileBoundingBox = new Rectangle(xPos, yPos, GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		tileSprite = new ImageIcon(TILE_ICE_SPRITE_FILENAME).getImage();
		triggered = false;
	}
	
	public TileIce(TileIce oldTileIce)
	{
		walkable = true;
		tileBoundingBox = new Rectangle((int)(oldTileIce.getBoundingBox().getX()), (int)(oldTileIce.getBoundingBox().getY()), GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		tileSprite = new ImageIcon(TILE_ICE_SPRITE_FILENAME).getImage();
		triggered = false;
	}
	
	public void update() 
	{
	}

	//Move the player one more if possible in the same direction they entered.
	public void onEnter(Level level) 
	{
		int xDifference = (int)(level.getPlayer().getBoundingBox().getX() - level.getPreviousPlayerTile().getBoundingBox().getX());
		int yDifference = (int)(level.getPlayer().getBoundingBox().getY() - level.getPreviousPlayerTile().getBoundingBox().getY());

		if(level.getTileAt(new Point((int)(level.getPlayer().getBoundingBox().getX() + xDifference), (int)(level.getPlayer().getBoundingBox().getY() + yDifference))).isWalkable() && level.getTileAt(new Point((int)(level.getPlayer().getBoundingBox().getX() + xDifference), (int)(level.getPlayer().getBoundingBox().getY() + yDifference))).getBoundingBox().getX() != GlobalConstants.getNotANumber())
		{
			level.getPreviousPlayerTile().moveTo((int)(level.getPlayer().getBoundingBox().getX()), (int)(level.getPlayer().getBoundingBox().getY()));
			level.activateOnExitTile(level.getPreviousPlayerTile());
			level.movePlayer((int)(level.getPlayer().getBoundingBox().getX() + xDifference), (int)(level.getPlayer().getBoundingBox().getY() + yDifference));
			level.activateOnEnterTile(new TilePlayer((int)(level.getPlayer().getBoundingBox().getX()), (int)(level.getPlayer().getBoundingBox().getY())));
		}
	}

	public void onExit(Level level) 
	{		
	}

	public void onTriggerEnter(Level level) 
	{		
	}

	public BaseTile deepCopy() 
	{
		return new TileIce(this);
	}
	
}

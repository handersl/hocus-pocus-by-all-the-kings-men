package hocusPocusTesterPackage;

public class Main 
{
	public static void main(String[] args) 
	{
		//The engine that takes care of running the game.
		Engine gameEngine;
		gameEngine = new Engine();
		
		//Update and draw the game engine.
		while(gameEngine.isRunning())
		{
			gameEngine.update();
		}
	}
}

package main;

import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class UpdateGui extends JFrame
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Singleton
	private static UpdateGui instance;
	
	// GUI
	private JTextArea msgField;
	
	// ============================================================================================
	// Constructors
	// ============================================================================================
	
	private UpdateGui()
	{
		super("Hocus Pocus Updater");
		
		// Set-Up Frame
		this.setContentPane(buildMainPanel());
		this.setResizable(false);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.pack();
		this.setLocation(getCenterOfScreen());
		this.setVisible(true);		
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================

	public static UpdateGui getInstance()
	{
		if(instance == null)
		{
			instance = new UpdateGui();
		}
		
		return instance;
	}
	
	public void displayMessage(String newMessage)
	{
		msgField.append(newMessage + "\n");
		msgField.setCaretPosition(msgField.getDocument().getLength());
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private JPanel buildMainPanel()
	{
		// Initialize Message Text Area
		msgField = new JTextArea("Initializing... \n");
		msgField.setEditable(false);
		msgField.setLineWrap(true);
		msgField.setWrapStyleWord(true);
		
		JScrollPane msgScroller = new JScrollPane(msgField);
		msgScroller.setAlignmentX(CENTER_ALIGNMENT);
		msgScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		msgScroller.setPreferredSize(new Dimension(400, 200));
		
		JButton startButton = new JButton("Start");
		startButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent event) 
			{
				((JButton)event.getSource()).setEnabled(false);
				Updater.updateLevels();
			}
			
		});
		
		JButton exitButton = new JButton("Exit");
		exitButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent event) 
			{
				UpdateGui.getInstance().dispose();
			}
			
		});
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setAlignmentX(CENTER_ALIGNMENT);
		buttonPanel.add(startButton);
		buttonPanel.add(exitButton);
		
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		
		mainPanel.add(msgScroller);
		mainPanel.add(buttonPanel);
		
		return mainPanel;
	}
	
	private Point getCenterOfScreen()
	{
		GraphicsEnvironment gEnv = GraphicsEnvironment.getLocalGraphicsEnvironment();
		Point center = gEnv.getCenterPoint();
		
		center.x -= (this.getWidth() / 2);
		center.y -= (this.getHeight() / 2);
		
		return center;
	}
}

package main;

import java.io.File;
import java.io.FilenameFilter;

import xml.FileIO;
import xml.LevelKit;

public class Updater 
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	public static void updateLevels()
	{
		// Get Current Directory
		String projDir = System.getProperty("user.dir");
		File oldDirectory = new File(projDir + "\\old");
		File newDirectory = new File(projDir + "\\new");
		
		// Get Files
		String fileNames[] = oldDirectory.list(new FilenameFilter()
		{
			@Override
			public boolean accept(File dir, String name) 
			{
				if(name.endsWith(".xml"))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		});
		
		int numFiles = fileNames.length;
		UpdateGui.getInstance().displayMessage("Files in directory: " + numFiles + "\n");
		
		// Loop Through Files and Update
		for(int index = 0; index < numFiles; index++)
		{
			UpdateGui.getInstance().displayMessage("Updating " + fileNames[index] + "...");

			LevelKit kit = FileIO.loadFile(fileNames[index], oldDirectory);
			
			if(kit == null)
			{
				UpdateGui.getInstance().displayMessage("Error updating file.\n");	
				continue;
			}
			
			FileIO.saveLevel(kit, newDirectory);	
			UpdateGui.getInstance().displayMessage(fileNames[index] + " completed.\n");
		}
		
		UpdateGui.getInstance().displayMessage("Updates complete.");
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
}

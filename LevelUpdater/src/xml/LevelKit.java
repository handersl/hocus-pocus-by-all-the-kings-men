package xml;

import java.util.ArrayList;

public class LevelKit 
{
	public final static int CURRENT_VERSION = 3;
	
	private Level levelData;
	private Tile tileData[][][];
	private ArrayList<Trigger> triggers;
	
	// Version 1 Constructor
	public LevelKit(Level lData, Tile tData[][][])
	{
		this.levelData = lData;
		this.tileData = tData;
		
		// Create Empty Trigger Data
		this.triggers = new ArrayList<Trigger>(0);
	}
	
	// Version 2 Constructor
	public LevelKit(Level lData, Tile tData[][][], ArrayList<Trigger> triggers)
	{
		this.levelData = lData;
		this.tileData = tData;
		this.triggers = triggers;
	}
	
	public Level getLevelData()
	{
		return this.levelData;
	}
	
	public Tile[][][] getTileData()
	{
		return this.tileData;
	}
	
	public ArrayList<Trigger> getTriggers()
	{
		return this.triggers;
	}
}

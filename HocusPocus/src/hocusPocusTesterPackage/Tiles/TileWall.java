package hocusPocusTesterPackage.Tiles;

import java.io.InputStream;

import com.example.hocuspocus.GlobalConstants;
import com.example.hocuspocus.Level;

import android.graphics.BitmapFactory;
import android.graphics.Rect;

//A Tile Wall is a tile that can not be walked into.
public class TileWall extends BaseTile
{	
	//The Wall Tile art filename.
    private final static String TILE_WALL_SPRITE_FILENAME = "res/drawable/tilewall.png";
    
    InputStream wallStream = this.getClass().getClassLoader().getResourceAsStream(TILE_WALL_SPRITE_FILENAME);
	
	public TileWall()
	{
		walkable = false;
		tileSprite = BitmapFactory.decodeStream(wallStream);
		tileBoundingBox = new Rect(0, 0, GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		triggered = false;
	}
	
	public TileWall(int xPos, int yPos)
	{
		walkable = false;
		tileSprite = BitmapFactory.decodeStream(wallStream);
		tileBoundingBox = new Rect(xPos, yPos, xPos + GlobalConstants.getTileSize(), yPos + GlobalConstants.getTileSize());
		triggered = false;
	}
	
	public TileWall(TileWall oldTileWall)
	{
		walkable = false;
		tileSprite = BitmapFactory.decodeStream(wallStream);
		tileBoundingBox = new Rect((int)(oldTileWall.getBoundingBox().left), (int)(oldTileWall.getBoundingBox().top), (int)(oldTileWall.getBoundingBox().left) + GlobalConstants.getTileSize(), (int)(oldTileWall.getBoundingBox().top) + GlobalConstants.getTileSize());
		triggered = false;
	}
	
	public void update()
	{
	}
	
	public void onEnter(Level level) 
	{
	}

	public void onExit(Level level) 
	{
	}

	public void onTriggerEnter(Level level) 
	{
	}

	public BaseTile deepCopy() 
	{
		return new TileWall(this);
	}
}

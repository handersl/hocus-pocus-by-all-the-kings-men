package hocusPocusTesterPackage.Tiles;

import java.io.InputStream;

import com.example.hocuspocus.GlobalConstants;
import com.example.hocuspocus.Level;

import android.graphics.BitmapFactory;
import android.graphics.Rect;


//A Tile Walkable is a tile that can be walked into.
public class TileWalkable extends BaseTile
{	
	//The Walkable Tile art filename.
    private final static String TILE_WALKABLE_SPRITE_FILENAME = "res/drawable/tilewalkable.png";
    InputStream walkableStream = this.getClass().getClassLoader().getResourceAsStream(TILE_WALKABLE_SPRITE_FILENAME);
    
	//Create a Tile Walkable at (0,0).
	public TileWalkable()
	{
		walkable = true;
		tileSprite = BitmapFactory.decodeStream(walkableStream);
		tileBoundingBox = new Rect(0, 0, GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		triggered = false;
	}
	
	//Create a Tile Unwalkable at (xPos, yPos).
	public TileWalkable(int xPos, int yPos)
	{
		walkable = true;
		tileSprite = BitmapFactory.decodeStream(walkableStream);
		tileBoundingBox = new Rect(xPos, yPos, xPos + GlobalConstants.getTileSize(), yPos +GlobalConstants.getTileSize());
		triggered = false;
	}
	
	public TileWalkable(TileWalkable oldTileWalkable)
	{
		walkable = true;
		tileSprite = BitmapFactory.decodeStream(walkableStream);
		tileBoundingBox = new Rect((int)(oldTileWalkable.getBoundingBox().left), (int)(oldTileWalkable.getBoundingBox().top), (int)(oldTileWalkable.getBoundingBox().left) + GlobalConstants.getTileSize(), (int)(oldTileWalkable.getBoundingBox().top) + GlobalConstants.getTileSize());
		triggered = false;
	}
	
	public void update() 
	{
		//Do nothing.	
	}
	
	public void onEnter(Level level)
	{
		//Do Nothing.
	}

	public void onExit(Level level) 
	{
		//Do Nothing.
	}

	public void onTriggerEnter(Level level) 
	{
	}

	@Override
	public BaseTile deepCopy()
	{
		return new TileWalkable(this);
	}
}

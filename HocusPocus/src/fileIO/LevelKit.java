package fileIO;

public class LevelKit 
{
	public final static int CURRENT_VERSION = 1;
	
	private Level levelData;
	private Tile tileData[][][];
	
	public LevelKit(Level lData, Tile tData[][][])
	{
		this.levelData = lData;
		this.tileData = tData;
	}
	
	public Level getLevelData()
	{
		return this.levelData;
	}
	
	public Tile[][][] getTileData()
	{
		return this.tileData;
	}
}

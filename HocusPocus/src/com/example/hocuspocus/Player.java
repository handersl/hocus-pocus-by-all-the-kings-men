package com.example.hocuspocus;

import java.io.InputStream;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;

public class Player
{
	//This Rect. is where the player is and the amount of space the character
	//takes up.
	private Rect boundingBox;
	//The sprite for the Player.
	private Bitmap playerSprite;
	
	//The dimensions of the Player.
	private static final int PLAYER_WIDTH = 64;
	private static final int PLAYER_HEIGHT = 64;
	//The Player art filename.
    private static final String PLAYER_SPRITE_FILENAME = "res/drawable/chip.png";
    
	//Create the Player at (0,0) and load the image needed.
	public Player()
	{
		boundingBox = new Rect(0, 0, PLAYER_WIDTH, PLAYER_HEIGHT);
		
		InputStream playerStream = this.getClass().getClassLoader().getResourceAsStream(PLAYER_SPRITE_FILENAME);
		playerSprite = BitmapFactory.decodeStream(playerStream);
	}
	
	//Move the Player to (newXPosition,newYPosition).
	public void moveTo(int newXPosition, int newYPosition)
	{
		boundingBox = new Rect(newXPosition, newYPosition, newXPosition + PLAYER_WIDTH, newYPosition + PLAYER_HEIGHT);
	}
	
	public void update()
	{
	}
	
	public void draw(Canvas canvas) 
	{
		if(playerSprite != null)
		{
			canvas.drawBitmap(playerSprite, boundingBox.left, boundingBox.top, null); 
		}
	}
	
	public Rect getBoundingBox()
	{
		return boundingBox;
	}
}

package com.example.hocuspocus;

import hocusPocusTesterPackage.Tiles.BaseTile;
import hocusPocusTesterPackage.Tiles.TileWalkable;
import hocusPocusTesterPackage.Tiles.TileWall;

import java.util.ArrayList;

import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;

//The Level Panel class houses the tiles that are local to each other.
//When the level rotates these tiles stay at the same distance for
//one another.
public class LevelPanel
{
	//The origin of a Level Plane is the top left of all the tiles.
	//It is the local origin of the Level Plane.
	private Point origin;
	//The size of the Level Panel.  Units of tiles.
	private int width;
	private int height;

	//The point that this panel wants to move to,
	private Point destination;
	//The speed of the panel.
	private double speed;
	//Is this panel done moving.
	private boolean doneMoving;
	
	//The Rect area in pixels.
	private Rect boundingBoxArea;
	
	//The list of the actual tiles.  It is a one dimensional array so
	//math will be done to navigate it.
	private ArrayList<BaseTile> listOfTiles;
	
	//How close does a panel have to be to snap to position.
	private static final int THRESHOLD_TO_SNAP = 10;
	
	//Creates a Level Plane with all Tile Walkables.  W is the numer of tiles
	//across and h is the number of tiles down.
	public LevelPanel(int xOrigin, int yOrigin, int levelPanelW, int levelPanelH)
	{
		origin = new Point(xOrigin, yOrigin);
	
		destination = new Point(xOrigin, yOrigin);
		speed = 5;
		
		doneMoving = true;
		
		listOfTiles = new ArrayList<BaseTile>();
		
		width = levelPanelW;
		height = levelPanelH;
		
		boundingBoxArea = new Rect((int)origin.x, (int)origin.y, origin.x + width * GlobalConstants.getTileSize(), origin.y + height * GlobalConstants.getTileSize());
		
		int currentlyBeingFilledX = 0;
		int currentlyBeingFilledY = 0;
		for(int currentTileBeingFilled = 0; currentTileBeingFilled < width*height; currentTileBeingFilled++)
		{
			listOfTiles.add(new TileWalkable((int)(origin.x + currentlyBeingFilledX*GlobalConstants.getTileSize()), (int)(origin.y + currentlyBeingFilledY*GlobalConstants.getTileSize())));
			currentlyBeingFilledX++;
			if(currentlyBeingFilledX >= width)
			{
				currentlyBeingFilledX = 0;
				currentlyBeingFilledY++;
			}
		}
	}
	
	public LevelPanel(LevelPanel oldLevelPanel)
	{
		origin = new Point((int)(oldLevelPanel.getBoundingBoxArea().left), (int)(oldLevelPanel.getBoundingBoxArea().top));
		
		destination = new Point((int)(oldLevelPanel.getBoundingBoxArea().left), (int)(oldLevelPanel.getBoundingBoxArea().top));
		speed = 1;
		
		doneMoving = true;
		
		listOfTiles = new ArrayList<BaseTile>();
		
		width = oldLevelPanel.width;
		height = oldLevelPanel.height;
		
		boundingBoxArea = new Rect((int)origin.x, (int)origin.y, origin.x + width * GlobalConstants.getTileSize(), origin.y + height * GlobalConstants.getTileSize());
		
		for(int copyTileNumber = 0;copyTileNumber < oldLevelPanel.getListOfTiles().size(); copyTileNumber++)
		{
			BaseTile baseTile = oldLevelPanel.getListOfTiles().get(copyTileNumber);
			listOfTiles.add(baseTile.deepCopy());
		}
	}
	
	//Replace the tile at (xPos, yPos).  The position of the new tile
	//does not matter because it will be set by this method to fit where
	//it should belong.  The location is 1-indexed and is the number of tiles to move
	//from the origin of the Level Panel.
	public void addTile(int xPos, int yPos, BaseTile newTile)
	{
		newTile.moveTo((int)(origin.x + xPos * GlobalConstants.getTileSize()), (int)(origin.y + yPos* GlobalConstants.getTileSize()));
		listOfTiles.set(xPos + yPos*width, newTile);
	}
	
	//This fills the board with the wall tile if placed.
	public void fillWall()
	{
		listOfTiles.clear();
		int currentlyBeingFilledX = 0;
		int currentlyBeingFilledY = 0;
		for(int currentTileBeingFilled = 0; currentTileBeingFilled < width*height; currentTileBeingFilled++)
		{
			listOfTiles.add(new TileWall((int)(origin.x + currentlyBeingFilledX*GlobalConstants.getTileSize()), (int)(origin.y + currentlyBeingFilledY*GlobalConstants.getTileSize())));
			currentlyBeingFilledX++;
			if(currentlyBeingFilledX >= width)
			{
				currentlyBeingFilledX = 0;
				currentlyBeingFilledY++;
			}
		}
	}
	
	//Sets the panel to move towards this point.
	public void moveTo(int newXPosition, int newYPosition)
	{
		destination = new Point(newXPosition, newYPosition);
	}
	
	//This will move all the entire Level Panel to a new origin point.
	public void moveToImm(int newXPosition, int newYPosition)
	{
		int xPositionDifference = (int)(newXPosition - origin.x);
		int yPositionDifference = (int)(newYPosition - origin.y);
		
		origin = new Point(newXPosition, newYPosition);
		boundingBoxArea = new Rect((int)origin.x, (int)origin.y, origin.x + width * GlobalConstants.getTileSize(), origin.y + height * GlobalConstants.getTileSize());
	
		for(int tileNumber = 0;tileNumber < listOfTiles.size(); tileNumber++)
		{
			BaseTile baseTile = listOfTiles.get(tileNumber);
			baseTile.moveTo((int)(baseTile.getBoundingBox().left + xPositionDifference), (int)(baseTile.getBoundingBox().top + yPositionDifference));
		}
	}
	
	public void update()
	{
		for(int tileNumber = 0;tileNumber < listOfTiles.size(); tileNumber++)
		{
			BaseTile baseTile = listOfTiles.get(tileNumber);
			baseTile.update();
		}
		
		//Move the level panel by its speed towards its point.
		if(!destination.equals(origin))
		{
			doneMoving = false;
			double xDiff = destination.x - origin.x;
			double yDiff = destination.y - origin.y;

			//Move in the y direction.
			if(Double.compare(xDiff, 0.0) == 0)
			{
				int sign = 1;
				if(yDiff < 0)
				{
					sign = -1;
				}
				origin = new Point((int)(origin.x), (int)(origin.y + speed*sign));
				boundingBoxArea = new Rect((int)origin.x, (int)origin.y, origin.x + width * GlobalConstants.getTileSize(), origin.y + height * GlobalConstants.getTileSize());
			
				for(int tileNumber = 0;tileNumber < listOfTiles.size(); tileNumber++)
				{
					BaseTile baseTile = listOfTiles.get(tileNumber);
					baseTile.moveTo((int)(baseTile.getBoundingBox().left), (int)(baseTile.getBoundingBox().top + speed*sign));
				}
				if(Math.abs(destination.y - origin.y) < THRESHOLD_TO_SNAP)
				{
					moveToImm((int)destination.x, (int)destination.y);
				}
			}
			//Move in the x direction.
			else if(Double.compare(yDiff, 0.0) == 0)
			{
				int sign = 1;
				if(xDiff < 0)
				{
					sign = -1;
				}
				origin = new Point((int)(origin.x + speed*sign), (int)(origin.y));
				boundingBoxArea = new Rect((int)origin.x, (int)origin.y, origin.x + width * GlobalConstants.getTileSize(), origin.y + height * GlobalConstants.getTileSize());
			
				for(int tileNumber = 0;tileNumber < listOfTiles.size(); tileNumber++)
				{
					BaseTile baseTile = listOfTiles.get(tileNumber);
					baseTile.moveTo((int)(baseTile.getBoundingBox().left + speed*sign), (int)(baseTile.getBoundingBox().top));
				}
				
				if(Math.abs(destination.x - origin.x) < THRESHOLD_TO_SNAP)
				{
					moveToImm((int)destination.x, (int)destination.y);
				}
			}
		}
		else
		{
			doneMoving = true;
		}
	}

	public void draw(Canvas canvas) 
	{
		for(int tileNumber = 0;tileNumber < listOfTiles.size(); tileNumber++)
		{
			BaseTile baseTile = listOfTiles.get(tileNumber);
			baseTile.draw(canvas);
		}
	}
	
	public LevelPanel deepCopy()
	{
		return new LevelPanel(this);
	}
	
	public ArrayList<BaseTile> getListOfTiles()
	{
		return listOfTiles;
	}
	
	public Rect getBoundingBoxArea()
	{
		return boundingBoxArea;
	}
	
	public int getWidthInTiles()
	{
		return width;
	}
	
	public int getHeightInTiles()
	{
		return height;
	}
	
	public boolean isDoneMoving()
	{
		return doneMoving;
	}
	
	public Point getDestination()
	{
		return destination;
	}
}

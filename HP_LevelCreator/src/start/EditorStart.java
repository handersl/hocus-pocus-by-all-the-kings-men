// ================================================================================================
// Class: EditorStart
// Primary Author: Travis Smith
// Last Modified: March 19, 1014
// ================================================================================================
// EditorStart will contain the main method where execution will start for the program. It will
// create an instance of the MainWindow class and allow the Swing Event thread to take over. If
// in later versions, data must be retrieved from the system or user, it should happen in this
// class before starting the MainWindow.
// ================================================================================================

package start;

//Imports
import javax.swing.SwingUtilities;

public class EditorStart
{

	// ===== main =================================================================================
	// The main function will create and instance of the MainWindow object. If data is needed from 
	// the system or user, the main method will handle that before creating the MainWindow.
	//
	// Input: String args[]		-	this should not be used or needed
	//
	// Output: none
	// ============================================================================================
	public static void main(String args[])
	{
		SwingUtilities.invokeLater(new Runnable()	// Invoke new runnable on the SwingEvent thread
		{
			@Override
			public void run() 
			{
				EditorMain.getInstance();
			}
			
		});
	}
}

package start;

import tester.view.TesterFrame;
import fileIO.FileIO;
import fileIO.LevelKit;
import gui.MainWindow;
import gui.mainLevelView.MapPanel;
import gui.modules.ImageControl;
import gui.modules.PathingModule;
import gui.modules.TileModule;
import gui.modules.TriggerModule;

public class EditorMain 
{
	// ============================================================================================
	// Data Members
	// ============================================================================================

	/** The singleton instance of the EditorMain class. */
	static private EditorMain instance = null;
	
	// Modes
	public static final int PATHING = 0; 
	public static final int TILES = 1; 
	public static final int TRIGGERS = 2; 
	public static final int ENEMIES = 3; 	
	private int currentMode;
	
	// Modules
	private PathingModule pathingModule;
	private TileModule tileModule;
	private TriggerModule triggerModule;

	// Tester
	private TesterFrame testFrame;
	
	// Main GUI
	private MainWindow mainWindow;
	private MapPanel mapPanel;
	@SuppressWarnings("unused")
	private ImageControl imageControl;
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================

	/**
	 * Returns the single instance of the EditorMain class, and provide access to its' interface. If 
	 * the instance has not been created yet, the method will create a new instance and return it.
	 * <p>
	 * @return 
	 * the singleton EditorMain instance
	 */
	static public EditorMain getInstance()
	{
		if(instance == null)
		{
			instance = new EditorMain();
		}
		
		return instance;
	}

	// Menu Button Clicks
	public void newLevelClicked()
	{
		mapPanel.newLevel();
	}
	
	public void loadLevelClicked()
	{
		LevelKit newKit = FileIO.loadLevel();
		
		if(newKit != null)
		{
			mapPanel.levelLoaded(newKit.getLevelData(), newKit.getTileData());
			triggerModule.loadedLevel(newKit.getLevelData().getNextTriggerID(), newKit.getTriggers());
		}
	}
	
	public void saveLevelClicked()
	{
		LevelKit saveKit = new LevelKit(mapPanel.getLevelData(), mapPanel.getAllTileData(), triggerModule.getTriggers());
		
		FileIO.saveLevel(saveKit);		
	}
	
	public void saveLevelAsClicked()
	{
	}
	
	public void quitClicked()
	{
		// TODO
		// Add "are you sure", save etc
		testerClosed();
		mainWindow.dispose();
		System.exit(0);
	}
	
	public void propertiesClicked()
	{
		mapPanel.editLevel();
	}
	
	public void testLevelClicked()
	{
		if(testFrame == null)
		{			
			LevelKit testKit = new LevelKit(mapPanel.getLevelData(), mapPanel.getAllTileData(), triggerModule.getTriggers());
			FileIO.saveLevel(testKit);	
			
			testFrame = new TesterFrame(testKit);
			
			mainWindow.add(testFrame);
			testFrame.setVisible(true);
			testFrame.initScreen();
			testFrame.start();
		}
	}
	
	public void testerClosed()
	{
		if(testFrame != null)
		{
			testFrame.stop();
			testFrame.setVisible(false);
			mainWindow.remove(testFrame);
			testFrame.dispose();
			testFrame = null;
		}
	}
	
	public int getCurrentMode()
	{
		return currentMode;
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	/**
	 * Class constructor. Will initialize all GUI members and prepare the MapPanel for user 
	 * interaction.
	 */
	private EditorMain()
	{
		imageControl = new ImageControl();
		mainWindow = new MainWindow();
		pathingModule = new PathingModule();
		tileModule = new TileModule();
		triggerModule = new TriggerModule();
		mapPanel = new MapPanel();
		
		mainWindow.addModule(pathingModule);
		mainWindow.addModule(tileModule);
		mainWindow.addModule(triggerModule);
		mainWindow.add(mapPanel);
		
		setMode(PATHING);
	}
	
	public void setMode(int mode)
	{
		if(mode == PATHING)
		{
			// Hide Other Modules
			tileModule.setVisible(false);
			triggerModule.setVisible(false);
			
			// Switch to New Module
			currentMode = PATHING;
			pathingModule.setVisible(true);
			
		}
		else if(mode == TILES)
		{
			// Hide Other Modules
			pathingModule.setVisible(false);
			triggerModule.setVisible(false);
			
			// Switch to New Module
			currentMode = TILES;
			tileModule.setVisible(true);			
		}
		else if(mode == TRIGGERS)
		{
			// Hide Other Modules
			tileModule.setVisible(false);
			pathingModule.setVisible(false);
			
			// Switch to New Module
			currentMode = TRIGGERS;
			triggerModule.setVisible(true);		
		}
		else if(mode == ENEMIES)
		{
			
		}
	}

	public char getSelectedTileType() 
	{
		return tileModule.getSelectedType();
	}
	
	public MapPanel getMapPanel()
	{
		return mapPanel;
	}
	
	public TriggerModule getTriggerModule()
	{
		return triggerModule;
	}

}

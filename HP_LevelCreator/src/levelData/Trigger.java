package levelData;

import java.util.ArrayList;

public class Trigger 
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Type Constants
	public final static int ACTIVATING = 0;
	public final static int DEACTIVATING = 1;
	public final static int TOGGLE = 2;
	
	public final static int ON_ENTER = 0;
	public final static int ON_EXIT = 1;
	public final static int BLOCK_ACTIVATED = 2;

	private int idNumber;
	private int type;
	private int whenTriggers;
	private int layer;
	private int xCord;
	private int yCord;
	
	private ArrayList<Target> targets;
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	public Trigger(int idNumber, int type, int whenTriggers, int layer, int xCord, int yCord)
	{
		this.idNumber = idNumber;
		this.type = type;
		this.whenTriggers = whenTriggers;
		this.layer = layer;
		this.xCord = xCord;
		this.yCord = yCord;
		
		this.targets = new ArrayList<Target>(0);
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	public Trigger copy()
	{
		Trigger copy = new Trigger(idNumber, type, whenTriggers, layer, xCord, yCord);
		
		for(int index = 0; index < targets.size(); index ++)
		{
			int target[] = {targets.get(index).layer, targets.get(index).xCord, targets.get(index).yCord};
			copy.addTarget(target);
		}

		return copy;
	}
	
	public boolean addTarget(int newLoc[])
	{
		Target newTarget = new Target(newLoc[0], newLoc[1], newLoc[2]);
		
		// Check if Target Exists
		for(int index = 0; index < targets.size(); index++)
		{
			Target currentTarget = targets.get(index);
			
			if((currentTarget.layer == newTarget.layer) &&
				(currentTarget.xCord == newTarget.xCord) &&
				(currentTarget.yCord == newTarget.yCord))
			{
				// Tile already registered as target, ignore
				return false;
			}
		}
		
		// Add Target
		targets.add(newTarget);
		return true;
	}
	
	public boolean removeTarget(int targetLoc[])
	{
		Target removeTarget = new Target(targetLoc[0], targetLoc[1], targetLoc[2]);
		
		// Check if Target Exists
		for(int index = 0; index < targets.size(); index++)
		{
			Target currentTarget = targets.get(index);
			
			if((currentTarget.layer == removeTarget.layer) &&
					(currentTarget.xCord == removeTarget.xCord) &&
					(currentTarget.yCord == removeTarget.yCord))
			{
				targets.remove(currentTarget);
				return true;
			}
		}
		
		return false;
	}
	
	public int[][] getTargetArray()
	{
		int targetArray[][] = new int[targets.size()][3];
		
		for(int index = 0; index < targets.size(); index++)
		{
			targetArray[index] = targets.get(index).getLocation();
		}
		
		return targetArray;
	}
	
	// Getters
	public int getIdNumber()
	{
		return idNumber;
	}
	public int getType() 
	{
		return type;
	}
	public int getWhenTriggers()
	{
		return whenTriggers;
	}
	public int getLayer() 
	{
		return layer;
	}
	public int[] getLocation() 
	{
		int location[] = new int[]{layer, xCord, yCord};
		
		return location;
	}
	public ArrayList<Target> getTargets() 
	{
		return targets;
	}
	
	// Setters
	public void setType(int type) 
	{
		this.type = type;
	}
	public void setLayer(int layer) 
	{
		this.layer = layer;
	}
	public void setLocation(int[] location) 
	{
		this.layer = location[0];
		this.xCord = location[1];
		this.yCord = location[2];
	}
	
	// ============================================================================================
	// Contained Classes
	// ============================================================================================
	
	public class Target
	{
		private int layer;
		private int xCord;
		private int yCord;
		
		public Target(int layer, int xCord, int yCord)
		{
			this.layer = layer;
			this.xCord = xCord;
			this.yCord = yCord;
		}
		
		public int getLayer()
		{
			return this.layer;
		}
		
		public int[] getLocation()
		{
			int location[] = new int[]{layer, xCord, yCord};
			
			return location;
		}
	}
	
}

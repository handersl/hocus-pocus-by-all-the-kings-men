package levelData;

public class Tile 
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Number of Types
	public static final int NUM_TYPES = 11;	// Needs to be incremented as types are added
	
	// Basic Tile Type Constants
	public static final char WALL_TILE = 'a';
	public static final char START_TILE = 'b';
	public static final char KEY_TILE = 'c';
	public static final char EXIT_TILE = 'd';
	public static final char FLOOR_TILE = 'e';
	public static final char ICE_TILE = 'f';
	public static final char SPIKE_TILE = 'g';
	public static final char BLOCK_TILE = 'h';
	public static final char BLOCK_SWITCH = 'i';
	
	// Special Tile Type Constants
	public static final char TELE_TILE = 'A';
	public static final char STAIRS_TILE = 'B';
	
	// Core Data
	private char type;
	private int numParameters;
	private int parameters[];
		
	// ============================================================================================
	// Constructors
	// ============================================================================================
	
	public Tile(char type, int numParams, int params[])
	{
		this.type = type;
		this.numParameters = numParams;
		this.parameters = params;
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	public Tile copy()
	{
		int newParams[] = new int[numParameters];
		
		for(int index = 0; index < numParameters; index++)
		{
			newParams[index] = parameters[index];
		}
		
		return new Tile(type, numParameters, newParams);
	}
	
	// Basic Tile Factory Method
	public static Tile buildBasicTile(char type)
	{
		int numParams = 5;
		int params[];
		
		if((type >= 'a') && (type <= 'z'))	// Basic Tile Range
		{			
			if(type == Tile.WALL_TILE) // Close Pathing
			{
				params = new int[]{1, 1, 1, 1, 1};
			}
			else	// Open Pathing
			{	
				params = new int[]{1, 0, 0, 0, 0};
			}
			
			Tile newData = new Tile(type, numParams, params);
			
			return newData;
		}
		else
		{
			// Error in type return a wall
			params = new int[]{1, 1, 1, 1, 1};
			return new Tile(Tile.WALL_TILE, numParams, params);
		}
	}
	
	// Getters
	public char getType()
	{
		return type;
	}
	
	public int getNumParameters()
	{
		return numParameters;
	}
	
	public int[] getParameters()
	{
		return parameters;
	}
}

package tester.models.level;

import java.util.ArrayList;

import levelData.Trigger;
import tester.models.Location;

public class GameTrigger 
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Type and triggering mode
	private int type;
	private int mode;
	
	// Location
	private Location location;
	
	// Targets
	private ArrayList<Location> targets;
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	protected GameTrigger(Trigger kit)
	{
		this.type = kit.getType();
		this.mode = kit.getWhenTriggers();
		
		int kitLoc[] = kit.getLocation();		
		this.location = new Location(kitLoc[0], kitLoc[1], kitLoc[2]);
			
		int numTargets = kit.getTargets().size();
		targets = new ArrayList<Location>(numTargets);
		
		for(int tIndex = 0; tIndex < numTargets; tIndex++)
		{
			int targetLoc[] = kit.getTargets().get(tIndex).getLocation();
			targets.add(new Location(targetLoc[0], targetLoc[1], targetLoc[2]));
		}
	}
	
	// ============================================================================================
	// Protected Methods
	// ============================================================================================
	
	protected void rotateTargets(boolean clockwise, int levelDim, int sectionDim)
	{
		for(int index = 0; index < targets.size(); index++)
		{
			// Get panel coordinates, rotate them
			int panelPos[] = getPanelCoordinates(targets.get(index).xCoordinate, targets.get(index).yCoordinate, sectionDim);
			
			int newPos[] = new int[4];
			
			// Transpose Panel
			newPos[0] = panelPos[1];
			newPos[1] = panelPos[0];
			newPos[2] = panelPos[2];
			newPos[3] = panelPos[3];
			
			if(clockwise)
			{
				// Reverse Col
				newPos[0] = (levelDim - newPos[0] - 1);
			}
			else
			{
				// Reverse Row
				newPos[1] = (levelDim - newPos[1] - 1);
			}
			
			int lvlLoc[] = getLevelCoordinates(newPos, sectionDim);
			
			targets.get(index).xCoordinate = lvlLoc[0];
			targets.get(index).yCoordinate = lvlLoc[1];
		}
	}
	
	protected ArrayList<Location> getTargetArray()
	{
		return targets;
	}
	
	protected Location getLocation()
	{
		return location;
	}
	
	protected int getType()
	{
		return type;
	}
	
	protected int getTriggerMode()
	{
		return mode;
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private int[] getPanelCoordinates(int levelCol, int levelRow, int sectionDim)
	{
		int panelPosition[]= new int[4];
		
		panelPosition[0] = (levelCol / sectionDim);
		panelPosition[1] = (levelRow / sectionDim);
		
		panelPosition[2] = (levelCol - (panelPosition[0] * sectionDim));
		panelPosition[3] = (levelRow - (panelPosition[1] * sectionDim));
		
		return panelPosition;
	}
	
	private int[] getLevelCoordinates(int panelCoordinates[], int sectionDim)
	{
		int levelCoord[] = new int[2];
		
		levelCoord[0] = (panelCoordinates[2] + (panelCoordinates[0] * sectionDim));
		levelCoord[1] = (panelCoordinates[3] + (panelCoordinates[1] * sectionDim));
		
		return levelCoord;
	}
}

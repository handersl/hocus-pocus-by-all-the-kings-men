package tester.view;

import gui.modules.ImageControl;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import levelData.Tile;
import tester.models.Location;

@SuppressWarnings("serial")
public class TesterScreen extends Canvas
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Size Constants
	public static final int DISPLAY_WIDTH = 640;
	public static final int DISPLAY_HEIGHT = 480;
	public static final int TILE_WIDTH = 64;
	public static final int TILE_HEIGHT = 64;
	
	// Display Data
	private final BufferedImage tileSheet;
	private BufferedImage mainImage;
	private BufferedImage backBuffer;
	private BufferStrategy bufferStrat;
	
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	public TesterScreen()
	{
		this.tileSheet = ImageControl.getTileSheet();
		this.mainImage = null;
		this.backBuffer = new BufferedImage(DISPLAY_WIDTH, DISPLAY_HEIGHT, BufferedImage.TYPE_INT_ARGB);
		this.bufferStrat = null;

		Dimension screenSize = new Dimension(DISPLAY_WIDTH, DISPLAY_HEIGHT);
		this.setMinimumSize(screenSize);
		this.setPreferredSize(screenSize);
		this.setMaximumSize(screenSize);
		
		this.setIgnoreRepaint(true);	
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	public void initializeBufferStrategy()
	{
		this.createBufferStrategy(3);
		this.bufferStrat = this.getBufferStrategy();	
	}
	
	public void render(Location playerLoc, ArrayList<Location> blocks)
	{
		if((mainImage == null) || (bufferStrat == null) || (playerLoc == null))
		{
			return;
		}
		
		// Get center pixel coordinates for player
		int playerCenterX = ((playerLoc.xCoordinate * TILE_WIDTH) + (TILE_WIDTH /2 ));
		int playerCenterY = ((playerLoc.yCoordinate * TILE_HEIGHT) + (TILE_WIDTH /2 ));
		
		// Get portion of main image to be drawn and ensure it is within level bounds
		int screenStartX = (playerCenterX - (DISPLAY_WIDTH / 2));
		int screenStartY = (playerCenterY - (DISPLAY_HEIGHT / 2));

		if((screenStartX < 0) || (mainImage.getWidth() < DISPLAY_WIDTH))
		{
			screenStartX = 0;
		}
		else if((screenStartX + DISPLAY_WIDTH) > mainImage.getWidth())
		{
			screenStartX = (mainImage.getWidth() - DISPLAY_WIDTH);
		}
		
		if((screenStartY < 0) || (mainImage.getHeight() < DISPLAY_HEIGHT))
		{
			screenStartY = 0;
		}
		else if((screenStartY + DISPLAY_HEIGHT) > mainImage.getHeight())
		{
			screenStartY = (mainImage.getHeight() - DISPLAY_HEIGHT);
		}
		
		Graphics bufferGfx = backBuffer.createGraphics();
		
		bufferGfx.setColor(Color.BLACK);
		bufferGfx.fillRect(0, 0, backBuffer.getWidth(), backBuffer.getHeight());
		
		// Draw background centered on player's pixel coordinates
		bufferGfx.drawImage(mainImage, 0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT,
							screenStartX, screenStartY, 
							(screenStartX + DISPLAY_WIDTH), (screenStartY + DISPLAY_HEIGHT), null);
		
		// Convert level coordinates to screen coordinates for player
		int playerScreenX = (playerCenterX - screenStartX - (TILE_WIDTH / 2));
		int playerScreenY = (playerCenterY - screenStartY - (TILE_HEIGHT / 2));
		
		Point playerImgLoc = ImageControl.getPlayerImageCoordinates();
		
		// Draw Player
		bufferGfx.drawImage(tileSheet, playerScreenX, playerScreenY,
					 (playerScreenX + 64), (playerScreenY + 64), 
					 (playerImgLoc.x * 64), (playerImgLoc.y * 64),
					 ((playerImgLoc.x + 1) * 64), ((playerImgLoc.y + 1) * 64), null);
		
		// Draw Blocks
		if(blocks != null)
		{
			for(int index = 0; index < blocks.size(); index++)
			{
				// Convert level coordinates to screen coordinates for Block
				int blockScreenX = ((blocks.get(index).xCoordinate * 64) - screenStartX);
				int blockScreenY = ((blocks.get(index).yCoordinate * 64) - screenStartY);
				Point imgLoc = ImageControl.lookupImageCoordinates(Tile.BLOCK_TILE);
				
				bufferGfx.drawImage(tileSheet,
									(blockScreenX),
									(blockScreenY),
									(blockScreenX + 64),
									(blockScreenY + 64), 
									(imgLoc.x * TILE_WIDTH),
									(imgLoc.y * TILE_HEIGHT),
									((imgLoc.x + 1) * TILE_WIDTH),
									((imgLoc.y + 1) * TILE_HEIGHT),
									null);
			}
		}
		
		// Draw back buffer to buffer strategy
		Graphics gfx = bufferStrat.getDrawGraphics();
		
		gfx.drawImage(backBuffer, 0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT,
					  0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT, null);

		bufferStrat.show();
		
		bufferGfx.dispose();
		gfx.dispose();		
	}
	
	@Override 
	public void paint(Graphics g)
	{
		render(null, null);
	}
	
	public void updateMainImage(char displayArray[][], int dimension)
	{
		mainImage = new BufferedImage((dimension * TILE_WIDTH), (dimension * TILE_HEIGHT), BufferedImage.TYPE_INT_RGB);
		
		Graphics2D gfx = mainImage.createGraphics();
		
		int colOffset = 0;
		int rowOffset = 0;
		
		for(int rIndex = 0; rIndex < dimension; rIndex++)
		{
			for(int cIndex = 0; cIndex < dimension; cIndex++)
			{
				Point imgLoc = ImageControl.lookupImageCoordinates(displayArray[cIndex][rIndex]);
				
				gfx.drawImage(tileSheet, colOffset, rowOffset, (colOffset + TILE_WIDTH), (rowOffset + TILE_HEIGHT), 
							 (imgLoc.x * TILE_WIDTH), (imgLoc.y * TILE_HEIGHT), ((imgLoc.x + 1) * TILE_WIDTH),
							 ((imgLoc.y + 1) * TILE_HEIGHT), null);

				colOffset += TILE_WIDTH;
			}
			
			colOffset = 0;
			rowOffset += TILE_HEIGHT;
		}
		
		gfx.dispose();
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
   
}

package tester.controller;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

@SuppressWarnings("serial")
public class InputHandler 
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Input Constants
	public static final int NONE = (-1);
	public static final int ESC_KEY = 0;
	public static final int W_KEY = 1;
	public static final int A_KEY = 2;
	public static final int S_KEY = 3;
	public static final int D_KEY = 4;
	public static final int Z_KEY = 5;
	public static final int X_KEY = 6;
	public static final int ENTER_KEY = 7;
	
	// Last Input
	private int lastInput;
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	public InputHandler()
	{
		lastInput = NONE;
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================

	public synchronized int getLastInput()
	{
		int last = lastInput;
		lastInput = NONE;
		
		return last;
	}
	
	public void createInputMap(JPanel contentPane)
	{
		InputMap inputMap = contentPane.getInputMap();
		ActionMap actionMap = contentPane.getActionMap();
		
		Action escapePressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{			
				setLastInput(ESC_KEY);
			}
		};
		Action enterPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				setLastInput(ENTER_KEY);
			}
		};
		Action wPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				setLastInput(W_KEY);
			}
		};
		
		Action aPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				setLastInput(A_KEY);
			}
		};
		
		Action sPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				setLastInput(S_KEY);
			}
		};
		
		Action dPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				setLastInput(D_KEY);
			}
		};
		
		Action zPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				setLastInput(Z_KEY);
			}
		};
		
		Action xPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				setLastInput(X_KEY);
			}
		};
		
		inputMap.put(KeyStroke.getKeyStroke("ESCAPE"), "Escape Pressed");
		inputMap.put(KeyStroke.getKeyStroke("ENTER"), "Enter Pressed");
		inputMap.put(KeyStroke.getKeyStroke('w'), "W Pressed");
		inputMap.put(KeyStroke.getKeyStroke('a'), "A Pressed");
		inputMap.put(KeyStroke.getKeyStroke('s'), "S Pressed");
		inputMap.put(KeyStroke.getKeyStroke('d'), "D Pressed");
		inputMap.put(KeyStroke.getKeyStroke("UP"), "W Pressed");
		inputMap.put(KeyStroke.getKeyStroke("LEFT"), "A Pressed");
		inputMap.put(KeyStroke.getKeyStroke("DOWN"), "S Pressed");
		inputMap.put(KeyStroke.getKeyStroke("RIGHT"), "D Pressed");
		inputMap.put(KeyStroke.getKeyStroke('z'), "Z Pressed");
		inputMap.put(KeyStroke.getKeyStroke('x'), "X Pressed");
		
		actionMap.put("Escape Pressed", escapePressed);
		actionMap.put("Enter Pressed", enterPressed);
		actionMap.put("W Pressed", wPressed);
		actionMap.put("A Pressed", aPressed);
		actionMap.put("S Pressed", sPressed);
		actionMap.put("D Pressed", dPressed);	
		actionMap.put("Z Pressed", zPressed);
		actionMap.put("X Pressed", xPressed);	

	}
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private synchronized void setLastInput(int input)
	{
		lastInput = input;
	}	
}

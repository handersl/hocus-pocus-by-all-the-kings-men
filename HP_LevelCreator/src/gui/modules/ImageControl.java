package gui.modules;

import gui.mainLevelView.MapButton;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import levelData.Tile;

public class ImageControl 
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Images
	static private final String EDITOR_TILES_PATH = "/editorTiles1.png";
	private static BufferedImage tileSheet;
	
	// ============================================================================================
	// Constructor
	// ============================================================================================

	public ImageControl()
	{
		try 
		{
			tileSheet = ImageIO.read(getClass().getResourceAsStream(EDITOR_TILES_PATH));
		} 
		catch (IOException e) 
		{
			System.err.println("Error Loading Editor Tiles: Exiting");
			System.exit(1);
		}		
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	/**
	 * Will lookup the image coordinates used to graphically display the passed tile type. These
	 * coordinates are hard-coded for now but should be changed to read a file that can be modified
	 * as needed by the artists.
	 * 
	 * @param tileType	- a char used to represent the type of tile
	 * 
	 * @return imageLoc	- a Point representing the (x, y) of the tile image relevant to the spritesheet.
	 * If the passed tile type cannot be found, will return (0, 0) instead.
	 */
	public static Point lookupImageCoordinates(char tileType)
	{
		if(tileType == Tile.WALL_TILE)
		{
			return new Point(0, 0);
		}
		else if(tileType == Tile.START_TILE)
		{
			return new Point(1, 0);
		}
		else if(tileType == Tile.KEY_TILE)
		{
			return new Point(2, 0);
		}
		else if(tileType == Tile.EXIT_TILE)
		{
			return new Point(3, 0);
		}
		else if(tileType == Tile.FLOOR_TILE)
		{
			return new Point(0, 1);
		}
		else if(tileType == Tile.ICE_TILE)
		{
			return new Point(1, 1);
		}
		else if(tileType == Tile.TELE_TILE)
		{
			return new Point(3, 1);
		}
		else if(tileType == Tile.STAIRS_TILE)
		{
			return new Point(2, 1);
		}
		else if(tileType == Tile.SPIKE_TILE)
		{
			// TODO
			return new Point(1, 3);
		}
		else if(tileType == Tile.BLOCK_TILE)
		{
			// TODO
			return new Point(3, 3);
		}
		else if(tileType == Tile.BLOCK_SWITCH)
		{
			// TODO
			return new Point(2, 3);
		}
		else
		{
			return new Point(0, 0);
		}
	}
	
	public static Point lookUpWallCoordinates(int side)
	{
		if(side == MapButton.TOP)
		{
			return new Point(0, 2);
		}
		else if(side == MapButton.RIGHT)
		{
			return new Point(1, 2);
		}
		else if(side == MapButton.BOTTOM)
		{
			return new Point(2, 2);
		}
		else if(side == MapButton.LEFT)
		{
			return new Point(3, 2);
		}
		else
		{
			return new Point(0, 0);
		}
	}
	
	public static Point getPlayerImageCoordinates()
	{
		return new Point(0, 3);
	}

	public static BufferedImage getTileSheet()
	{
		return tileSheet;
	}
}

package gui.tilepops;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import start.EditorMain;
import levelData.Tile;

/* ================================================================================================
 * This is the pop-up that will be used for Teleport tiles. The first new parameter is the Unique 
 * ID of this TP and the second is the ID of the destination TP.
 * 
 * params[5] - name of this TP
 * params[6] - name of destination
 * ============================================================================================= */

@SuppressWarnings("serial")
public class TeleportOptions extends BasicOptions
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	private static final int NUM_TP_PARAMS = 7;
	
	// Parameter Index Constants
	public static final int NAME = 5;
	public static final int DESTINATION = 6;
	
	private int uniqueID;

	// Basic GUI
	private JComboBox<Integer> destBox;
	
	// ============================================================================================
	// Constructors
	// ============================================================================================
	
	protected TeleportOptions() 
	{		
		super();
		
		// Teleporters add two new parameters
		this.numParams = NUM_TP_PARAMS;
		this.uniqueID = EditorMain.getInstance().getMapPanel().getNextTeleportID();
		
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(buildGUI());
	}
	
	protected TeleportOptions(Tile data) 
	{		
		super(data);
		
		// Teleporters add two new parameters
		this.numParams = NUM_TP_PARAMS;
		this.uniqueID = data.getParameters()[NAME];
		
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(buildGUI());
		
		setValues(data);
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================

	@Override
	public int[] getParams() 
	{
		int params[] = new int[numParams];
		
		int basicParams[] = super.getParams();
		
		// Add basic parameters to array
		for(int pIndex = 0; pIndex < BasicOptions.NUM_BASIC_PARAMS; pIndex++)
		{
			params[pIndex] = basicParams[pIndex];
		}
		
		// Dest
		params[NAME] = uniqueID;
		params[DESTINATION] = (int)destBox.getSelectedItem();
		
		return params;
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private JPanel buildGUI() 
	{
		// Build Destination Panel
		JPanel destPanel = new JPanel();
		destPanel.setBorder(BorderFactory.createTitledBorder("Teleporter #" + uniqueID + " Options"));
		JLabel destLabel = new JLabel("Destination: ");
		
		destBox = new JComboBox<Integer>(convertTeleIDs());
		
		destPanel.add(destLabel);
		destPanel.add(destBox);
		
		return destPanel;
	}
	
	private Integer[] convertTeleIDs()
	{
		int numTPs = EditorMain.getInstance().getMapPanel().getTeleporterListSize();
		int tpIds[] = EditorMain.getInstance().getMapPanel().getTeleporterIds();
		
		Integer destArray[] = new Integer[numTPs + 1];
		destArray[0] = (-1);

		for(int index = 0; index < numTPs; index++)
		{
			destArray[index + 1] = tpIds[index];
		}
		
		return destArray;
	}
	
	private void setValues(Tile data)
	{
		Integer destId = data.getParameters()[DESTINATION];
		
		destBox.setSelectedItem(destId);
	}
}

package gui.tilepops;

import levelData.Tile;

public class PopUpFactory 
{
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	public static BasicOptions buildNewTilePopUp(char type)
	{
		if((type >= 'a') && (type <= 'z'))	// Basic Tile, no need for pop-up
		{
			return buildBasicTilePopUp(null);
		}
		else
		{
			if(type == Tile.TELE_TILE)
			{
				return buildTeleportTilePopUp(null);
			}
			else if(type == Tile.STAIRS_TILE)
			{
				return buildStairsTilePopUp(null);
			}
			else
			{
				// Error with type
				return null;
			}
		}
	}
	
	public static BasicOptions buildEditTilePopUp(Tile data)
	{
		char type = data.getType();
		
		if((type >= 'a') && (type <= 'z'))	// Basic Tile, no need for pop-up
		{
			return buildBasicTilePopUp(data);
		}
		else
		{
			if(type == Tile.TELE_TILE)
			{
				return buildTeleportTilePopUp(data);
			}
			else if(type == Tile.STAIRS_TILE)
			{
				return buildStairsTilePopUp(data);
			}
			else
			{
				// Error with type
				return null;
			}
		}
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private static BasicOptions buildBasicTilePopUp(Tile data)
	{
		BasicOptions popUp;
		
		if(data == null)
		{
			popUp = new BasicOptions();
		}
		else
		{
			popUp = new BasicOptions(data);
		}

		return popUp;
	}
	
	private static BasicOptions buildTeleportTilePopUp(Tile data)
	{
		TeleportOptions popUp;
		
		if(data == null)
		{
			popUp = new TeleportOptions();
		}
		else
		{
			popUp = new TeleportOptions(data);
		}

		return popUp;
	}
	
	private static BasicOptions buildStairsTilePopUp(Tile data)
	{
		StairsOptions popUp;
		
		if(data == null)
		{
			popUp = new StairsOptions();
		}
		else
		{
			popUp = new StairsOptions(data);
		}

		return popUp;
	}
}

package gui.mainLevelView;

import gui.modules.ImageControl;
import gui.tilepops.BasicOptions;
import gui.tilepops.PopUpFactory;
import gui.tilepops.TeleportOptions;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import start.EditorMain;
import levelData.Tile;
import levelData.Trigger;

/**
The MapButton will represent each "grid square" of the levels. In the editor these buttons will
perform different actions based on the current editing mode and for right or left clicks.
<p>
The class will extend a JButton and use the swing library for graphics and mouse handling.

@author 	Travis Smith
@version 	1.0 | 18 April 2014
@see javax.swing.JButton
*/
@SuppressWarnings("serial")
public class MapButton extends JButton
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Size Constants
	public static final int BUTTON_WIDTH = 64;
	public static final int BUTTON_HEIGHT = 64;
	
	// Direction Constants
	public static final int TOP = 0;
	public static final int RIGHT = 1;
	public static final int BOTTOM = 2;
	public static final int LEFT = 3;
	
	// Highlights 
	public static final Color RED_HIGHLIGHT = new Color(255, 0, 0, 100);
	public static final Color GREEN_HIGHLIGHT = new Color(0, 255, 0, 100);
	public static final Color BLUE_HIGHLIGHT = new Color(0, 0, 255, 150);
	public static final Color YELLOW_HIGHLIGHT = new Color(255, 255, 0, 100);

	// Wall Highlight Boxes
	public static final int LONG_DIM = 32;
	public static final int SHORT_DIM = 16;
	public static final Rectangle TOP_BOX = new Rectangle(SHORT_DIM, 0, LONG_DIM, SHORT_DIM);
	public static final Rectangle RIGHT_BOX = new Rectangle((BUTTON_WIDTH - SHORT_DIM), SHORT_DIM, SHORT_DIM, LONG_DIM);
	public static final Rectangle BOTTOM_BOX = new Rectangle(SHORT_DIM, (BUTTON_HEIGHT - SHORT_DIM), LONG_DIM, SHORT_DIM);
	public static final Rectangle LEFT_BOX = new Rectangle(0, SHORT_DIM, SHORT_DIM, LONG_DIM);
	
	// Members
	private final int layer;
	private char tileType;
	private int numParameters;
	private int parameters[];
	private final Point gridCoord;
	private Point imageCoord;
	private int showHighlight;
	private boolean isTriggerTarget;
	private boolean isTrigger;
	private int triggerType;

	// ============================================================================================
	// Constructors
	// ============================================================================================
	
	protected MapButton(final int layer, final Point gridCoord)
	{
		// Initialize JButton
		super();		
		this.setPreferredSize(new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT));
		this.setMaximumSize(new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT));
		this.setMinimumSize(new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT));
		this.setFocusable(false);
		
		// Initialize Data
		this.layer = layer;
		setTileData(Tile.buildBasicTile(Tile.FLOOR_TILE));
		this.gridCoord = gridCoord;
		this.showHighlight = (-1);
		this.isTriggerTarget = false;
		this.isTrigger = false;
		this.triggerType = (-1);

		refresh();

		MapAdapter adapter = new MapAdapter();
		this.addMouseListener(adapter);
		this.addMouseMotionListener(adapter);
	}
	
	protected MapButton(final int layer, final Point gridCoord, Tile tileData)
	{
		// Initialize JButton
		super();		
		this.setPreferredSize(new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT));
		this.setMaximumSize(new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT));
		this.setMinimumSize(new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT));
		this.setFocusable(false);
		
		// Initialize Data
		this.layer = layer;
		this.gridCoord = gridCoord;
		this.showHighlight = (-1);
		this.isTriggerTarget = false;
		this.isTrigger = false;
		this.triggerType = (-1);
		setTileData(tileData);
		refresh();

		MapAdapter adapter = new MapAdapter();
		this.addMouseListener(adapter);
		this.addMouseMotionListener(adapter);
	}

	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	public void setTrigger(boolean state, int type)
	{
		isTrigger = state;
		triggerType = type;
		
		repaint();
	}
	
	public void setTriggerTarget(boolean state)
	{
		isTriggerTarget = state;

		repaint();
	}
	
	@Override
	public void paint(Graphics gfx)
	{
		BufferedImage tileImages = ImageControl.getTileSheet();
		if((tileImages == null) || (imageCoord == null))
		{
			gfx.setColor(Color.BLACK);
			gfx.drawRect(0, 0, (BUTTON_WIDTH - 1), (BUTTON_HEIGHT - 1));
		}
		else
		{
			gfx.drawImage(tileImages, 0, 0, (BUTTON_WIDTH), (BUTTON_HEIGHT)
						, (imageCoord.x * BUTTON_WIDTH), (imageCoord.y * BUTTON_HEIGHT)
						, (((imageCoord.x + 1) * BUTTON_WIDTH)), (((imageCoord.y + 1) * BUTTON_HEIGHT)), null);
		}
		
		// Draw Walls
		if(parameters[BasicOptions.NORTH] == 1)
		{
			Point wall = ImageControl.lookUpWallCoordinates(TOP);
			
			gfx.drawImage(tileImages, 0, 0, (BUTTON_WIDTH), (BUTTON_HEIGHT)
					, (wall.x * BUTTON_WIDTH), (wall.y * BUTTON_HEIGHT)
					, (((wall.x + 1) * BUTTON_WIDTH)), (((wall.y + 1) * BUTTON_HEIGHT)), null);			
		}
		if(parameters[BasicOptions.EAST] == 1)
		{
			Point wall = ImageControl.lookUpWallCoordinates(RIGHT);
			
			gfx.drawImage(tileImages, 0, 0, (BUTTON_WIDTH), (BUTTON_HEIGHT)
					, (wall.x * BUTTON_WIDTH), (wall.y * BUTTON_HEIGHT)
					, (((wall.x + 1) * BUTTON_WIDTH)), (((wall.y + 1) * BUTTON_HEIGHT)), null);			
		}
		if(parameters[BasicOptions.SOUTH] == 1)
		{
			Point wall = ImageControl.lookUpWallCoordinates(BOTTOM);
			
			gfx.drawImage(tileImages, 0, 0, (BUTTON_WIDTH), (BUTTON_HEIGHT)
					, (wall.x * BUTTON_WIDTH), (wall.y * BUTTON_HEIGHT)
					, (((wall.x + 1) * BUTTON_WIDTH)), (((wall.y + 1) * BUTTON_HEIGHT)), null);			
		}
		if(parameters[BasicOptions.WEST] == 1)
		{
			Point wall = ImageControl.lookUpWallCoordinates(LEFT);
			
			gfx.drawImage(tileImages, 0, 0, (BUTTON_WIDTH), (BUTTON_HEIGHT)
					, (wall.x * BUTTON_WIDTH), (wall.y * BUTTON_HEIGHT)
					, (((wall.x + 1) * BUTTON_WIDTH)), (((wall.y + 1) * BUTTON_HEIGHT)), null);			
		}
		
		// Draw Target Highlight
		if(EditorMain.TRIGGERS == EditorMain.getInstance().getCurrentMode())
		{
			if(isTriggerTarget)
			{
				gfx.setColor(BLUE_HIGHLIGHT);
				gfx.fillRect(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
			}
		}
		
		// Draw Trigger
		if(EditorMain.TRIGGERS == EditorMain.getInstance().getCurrentMode())
		{
			if(isTrigger)
			{
				if(triggerType == Trigger.ACTIVATING)
				{
					gfx.setColor(Color.GREEN);
				}
				else if(triggerType == Trigger.DEACTIVATING)
				{
					gfx.setColor(Color.RED);
				}
				else if(triggerType == Trigger.TOGGLE)
				{
					gfx.setColor(Color.ORANGE);
				}
				else
				{
					// Error
					gfx.setColor(Color.BLACK);
				}
				
				gfx.fillOval(16, 16, 32, 32);
			}
		}
		
		// Draw Wall Highlight
		if(EditorMain.PATHING == EditorMain.getInstance().getCurrentMode())
		{
			if(showHighlight == TOP)
			{
				gfx.setColor(BLUE_HIGHLIGHT);
				gfx.fillRect(TOP_BOX.x, TOP_BOX.y, TOP_BOX.width, TOP_BOX.height);
			}
			else if(showHighlight == RIGHT)
			{
				gfx.setColor(BLUE_HIGHLIGHT);
				gfx.fillRect(RIGHT_BOX.x, RIGHT_BOX.y, RIGHT_BOX.width, RIGHT_BOX.height);			
			}
			else if(showHighlight == BOTTOM)
			{
				gfx.setColor(BLUE_HIGHLIGHT);
				gfx.fillRect(BOTTOM_BOX.x, BOTTOM_BOX.y, BOTTOM_BOX.width, BOTTOM_BOX.height);			
			}
			else if(showHighlight == LEFT)
			{
				gfx.setColor(BLUE_HIGHLIGHT);
				gfx.fillRect(LEFT_BOX.x, LEFT_BOX.y, LEFT_BOX.width, LEFT_BOX.height);			
			}
		}
	}

	public Tile getTileData()
	{
		Tile tileData = new Tile(tileType, numParameters, parameters);
		
		return tileData;
	}
	
	public void setTileData(Tile data)
	{
		// Reset the Start/Exit if replacing it on tile
		if(tileType == Tile.START_TILE)
		{
			EditorMain.getInstance().getMapPanel().resetStart();
		}
		else if(tileType == Tile.EXIT_TILE)
		{
			EditorMain.getInstance().getMapPanel().resetExit();
		}
		
		if(tileType == Tile.TELE_TILE)	// Remove Teleporter From level list
		{
			this.setToolTipText(null);
			EditorMain.getInstance().getMapPanel().removeTeleporter(parameters[TeleportOptions.NAME]);
		}
		
		tileType = data.getType();
		numParameters = data.getNumParameters();

		parameters = new int[numParameters];
		
		for(int index = 0; index < numParameters; index++)
		{
			parameters[index] = data.getParameters()[index];
		}
		
		// Update Specific Tiles at Level
		if(tileType == Tile.START_TILE)
		{
			EditorMain.getInstance().getMapPanel().placeStart(layer, gridCoord);
		}
		else if(tileType == Tile.EXIT_TILE)
		{
			EditorMain.getInstance().getMapPanel().placeExit(layer, gridCoord);
		}
		else if(tileType == Tile.TELE_TILE)	// Add TP to level list
		{
			this.setToolTipText("Teleporter #" + parameters[TeleportOptions.NAME]);
			EditorMain.getInstance().getMapPanel().addTeleporter(parameters[TeleportOptions.DESTINATION]);
		}
		
		refresh();
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private void refresh()
	{
		imageCoord = ImageControl.lookupImageCoordinates(tileType);		
		repaint();
	}
	
	private void leftClicked(boolean shiftPressed, boolean ctrlPressed, Point mouseLoc)
	{
		int editingMode = EditorMain.getInstance().getCurrentMode();
		
		if(editingMode == EditorMain.PATHING)
		{
			if(ctrlPressed)
			{
				if(TOP_BOX.contains(mouseLoc))
				{
					parameters[BasicOptions.NORTH] = 0;
				}
				else if(RIGHT_BOX.contains(mouseLoc))
				{
					parameters[BasicOptions.EAST] = 0;
				}
				else if(BOTTOM_BOX.contains(mouseLoc))
				{
					parameters[BasicOptions.SOUTH] = 0;
				}
				else if(LEFT_BOX.contains(mouseLoc))
				{
					parameters[BasicOptions.WEST] = 0;
				}
			}
			else
			{
				if(shiftPressed)
				{
					EditorMain.getInstance().getMapPanel().setPathCorner(gridCoord, true);
				}
				else
				{
					EditorMain.getInstance().getMapPanel().resetPathCorners();
	
					Tile newData = Tile.buildBasicTile(Tile.FLOOR_TILE);
					setTileData(newData);				
				}
			}
			
			repaint();
		}
		else if(editingMode == EditorMain.TILES)
		{
			char newType = EditorMain.getInstance().getSelectedTileType();
			
			if(tileType != newType)
			{
				if((newType >= 'a') && (newType <= 'z'))	// Basic Tile, no need for pop-up
				{
					Tile newData = Tile.buildBasicTile(newType);
					
					setTileData(newData);
				}
				else
				{
					BasicOptions popup = PopUpFactory.buildNewTilePopUp(newType);
					
					// Create Pop-up and get user input
					int optionChoice = JOptionPane.showConfirmDialog(null, popup, "Setup Tile", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null);
					
					if(optionChoice == JOptionPane.OK_OPTION)
					{	
						Tile newData = new Tile(newType, popup.getNumParams(), popup.getParams());
						setTileData(newData);
					}		
				}
			}
		}
		else if(editingMode == EditorMain.TRIGGERS)
		{
			EditorMain.getInstance().getTriggerModule().leftClick(layer, gridCoord, shiftPressed);
		}
		else if(editingMode == EditorMain.ENEMIES)
		{
			// TODO
		}
		else
		{
			// Error in mode, do nothing
		}
	}
	
	private void rightClicked(boolean shiftPressed, boolean ctrlPressed, Point mouseLoc)
	{
		int editingMode = EditorMain.getInstance().getCurrentMode();
		
		if(editingMode == EditorMain.PATHING)
		{
			if(ctrlPressed)
			{
				if(TOP_BOX.contains(mouseLoc))
				{
					parameters[BasicOptions.NORTH] = 1;
				}
				else if(RIGHT_BOX.contains(mouseLoc))
				{
					parameters[BasicOptions.EAST] = 1;
				}
				else if(BOTTOM_BOX.contains(mouseLoc))
				{
					parameters[BasicOptions.SOUTH] = 1;
				}
				else if(LEFT_BOX.contains(mouseLoc))
				{
					parameters[BasicOptions.WEST] = 1;
				}
			}
			else
			{
				if(shiftPressed)
				{
					EditorMain.getInstance().getMapPanel().setPathCorner(gridCoord, false);
				}
				else
				{
					EditorMain.getInstance().getMapPanel().resetPathCorners();
					
					Tile newData = Tile.buildBasicTile(Tile.WALL_TILE);
					
					setTileData(newData);				
				}	
			}
			
			repaint();
		}
		else if(editingMode == EditorMain.TILES)
		{
			BasicOptions popup = PopUpFactory.buildEditTilePopUp(getTileData());
			
			// Create Pop-up and get user input
			int optionChoice = JOptionPane.showConfirmDialog(null, popup, "Edit Tile", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null);
			
			if(optionChoice == JOptionPane.OK_OPTION)
			{	
				parameters = popup.getParams();
			}		
		}
		else if(editingMode == EditorMain.TRIGGERS)
		{
			EditorMain.getInstance().getTriggerModule().rightClick(layer, gridCoord, shiftPressed);
		}
		else if(editingMode == EditorMain.ENEMIES)
		{
			// TODO
		}
		else
		{
			// Error in mode, do nothing
		}		
	}

	// ============================================================================================
	// Contained MouseAdapter Class
	// ============================================================================================

	/**
	The MapAdapter will extend a MouseAdapter and handle the inputs from the user. The inputs that
	will be handled are left and right clicks. 

	@author 	Travis Smith
	@version 	1.0 | 18 April 2014
	@see java.awt.event.MouseAdapter
	*/
	private class MapAdapter extends MouseAdapter
	{	
		@Override
		public void mousePressed(MouseEvent click) 
		{
			if(SwingUtilities.isLeftMouseButton(click))
			{
				leftClicked(click.isShiftDown(), click.isControlDown(), click.getPoint());
			}
			else if(SwingUtilities.isRightMouseButton(click))
			{
				rightClicked(click.isShiftDown(), click.isControlDown(), click.getPoint());
			}
		}

		@Override
		public void mouseMoved(MouseEvent event)
		{
			Point mouseLoc = event.getPoint();
			
			if(event.isControlDown())
			{
				if(TOP_BOX.contains(mouseLoc))
				{
					showHighlight = TOP;
				}
				else if(RIGHT_BOX.contains(mouseLoc))
				{
					showHighlight = RIGHT;
				}
				else if(BOTTOM_BOX.contains(mouseLoc))
				{
					showHighlight = BOTTOM;
				}
				else if(LEFT_BOX.contains(mouseLoc))
				{
					showHighlight = LEFT;
				}
				else
				{
					showHighlight = (-1);
				}
				
				repaint();
			}
		}
		
		@Override
		public void mouseExited(MouseEvent event)
		{
			showHighlight = (-1);
			repaint();
		}
		
		
	}
}

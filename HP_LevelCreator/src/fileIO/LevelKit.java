package fileIO;

import java.util.ArrayList;

import levelData.Level;
import levelData.Tile;
import levelData.Trigger;

public class LevelKit 
{
	public final static int CURRENT_VERSION = 3;
	
	private Level levelData;
	private Tile tileData[][][];
	private ArrayList<Trigger> triggers;
	
	public LevelKit(Level lData, Tile tData[][][], ArrayList<Trigger> triggers)
	{
		this.levelData = lData;
		this.tileData = tData;
		this.triggers = triggers;
	}
	
	public LevelKit copy()
	{
		ArrayList<Trigger> triggerCopies = new ArrayList<Trigger>(0);
		
		for(int index = 0; index < triggers.size(); index++)
		{
			triggerCopies.add(triggers.get(index).copy());
		}
		
		int totalDim = levelData.getLevelDimension() * levelData.getSectionDimension();
		
		Tile tileCopies[][][] = new Tile[levelData.getNumLayers()][totalDim][totalDim];
		
		for(int lIndex = 0; lIndex < levelData.getNumLayers(); lIndex++)	
		{
			for(int rIndex = 0; rIndex < totalDim; rIndex++)	
			{
				for(int cIndex = 0; cIndex < totalDim; cIndex++)	
				{
					tileCopies[lIndex][cIndex][rIndex] = tileData[lIndex][cIndex][rIndex].copy();
				}
			}
		}
		
		return new LevelKit(levelData.copy(), tileCopies, triggerCopies);
	}
	
	public Level getLevelData()
	{
		return this.levelData;
	}
	
	public Tile[][][] getTileData()
	{
		return this.tileData;
	}
	
	public ArrayList<Trigger> getTriggers()
	{
		return triggers;
	}
}
